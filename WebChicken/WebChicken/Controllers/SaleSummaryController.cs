﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Helpers;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    /// <summary>
    /// class to half item quantity.
    /// </summary>
    public class HalfItemQuantity
    {
        public int? Id { get; set; }
        public int Quantity { get; set; }
    }

    /// <summary>
    /// class to item quantity
    /// </summary>
    public class ItemQuantity
    {
        public int? Id { get; set; }
        public int? Quantity { get; set; }
        /// <summary>
        /// //constructor that add the half items to the count of the item.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="quantity"></param>
        /// <param name="halfQuantities"></param>
        public ItemQuantity(int? id, int? quantity, List<HalfItemQuantity> halfQuantities)
        {
            Id = id;
            Quantity = quantity;
            if (halfQuantities.Count() > 0 && quantity != null && halfQuantities.FirstOrDefault(hq => hq.Id == id) != null)
                Quantity += halfQuantities.FirstOrDefault(hq => hq.Id == id).Quantity / 2;
        }
    }

    /// <summary>
    /// class to summary table of the sale details
    /// </summary>
    [Route("api/saleSummary")]
    [ApiController]
    public class SaleSummaryController : ControllerBase
    {
        public const string Admin = "1";
        private readonly ChickenSaleContext _context;

        public SaleSummaryController(ChickenSaleContext context)
        {
            _context = context;
        }


        // GET: api/SaleSummary
        [HttpGet]
        public IEnumerable<TsaleItem> GetTsaleItem()
        {
            return _context.TsaleItem;
        }

        private int converNullToZero(object value)
        {
            if (value == null)
                return 0;
            return Convert.ToInt32(value);
        }

        /// <summary>
        /// get sale id and return summary table of the sale details
        /// </summary>
        /// <param name="saleId"></param>
        /// <returns></returns>
        // GET: api/SaleSummary/5
        [HttpGet("GetTsaleSummary/{saleId}")]
        [Authorize]
        public  IActionResult GetTsaleSummary(int saleId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //create a list of the half items quantities.
            var HalfQuantities = _context.TfamilyItemSale.Where(f => f.SaleItem.Item.ParentLinkId != null)
                .GroupBy(f1 => f1.SaleItem.Item.ParentLinkId)
                .Select(y => new HalfItemQuantity
                {
                    Id = y.Key,
                    Quantity = y.Sum(x => x.Quantity) ?? 0
                });

            //create a list of the items quantities.
            //with the itemId and quantity
            var itemsQuantities = _context.TfamilyItemSale.Include("SaleItem").Where(s => s.SaleItem.Item.ParentLinkId == null&& s.SaleItem.SaleId == saleId)
              //   var itemsQuantities = _context.TsaleItem.Include("Item").Where(s => s.Item.ParentLinkId == null && s.SaleId == saleId)
                .GroupBy(x => x.SaleItem.ItemId)
                .Select(y => new ItemQuantity(y.Key, y.Sum(x => x.Quantity), HalfQuantities.ToList())).ToList();

            var packagesToOrder = new[] { new { id = 0, quantity = 0, missing = 0 } }.ToList();
            foreach (var item in _context.TsaleItem.Include(x=>x.TfamilyItemSale).Where(s=>s.SaleId == saleId && s.TfamilyItemSale.Count()>0).Include("Item").ToList())
            {
                if (packagesToOrder?.First()?.id == 0 && packagesToOrder?.First()?.quantity == 0)
                    packagesToOrder.Remove(packagesToOrder.First());
                if (item.Item.ParentLinkId == null && itemsQuantities.FirstOrDefault(i => i.Id == item.ItemId) != null)
                {
                    if (item.ChangeOrderQuantity == null)
                        packagesToOrder.Add(new
                        {
                            id = item.ItemId ?? 0,
                            quantity = (itemsQuantities.FirstOrDefault(q => q.Id == item.ItemId).Quantity -
                            item.Extra ?? 0) /
                            _context.Titem.FirstOrDefault(i => i.ItemId == item.ItemId).UnitsInPackage ?? 1,
                            missing = (itemsQuantities.FirstOrDefault(q => q.Id == item.ItemId).Quantity -
                            item.Extra ?? 0) %
                            _context.Titem.FirstOrDefault(i => i.ItemId == item.ItemId).UnitsInPackage ?? 1
                        });
                    else if(item.ChangeOrderQuantity!=0)
                    {
                        packagesToOrder.Add(new
                        {
                            id = item.ItemId ?? 0,
                            quantity = item.ChangeOrderQuantity ?? 0,
                            missing = (itemsQuantities.FirstOrDefault(q => q.Id == item.ItemId).Quantity -
                            item.Extra ?? 1) % item.ChangeOrderQuantity ?? 0
                        });
                    }
                    else
                    {
                        packagesToOrder.Add(new
                        {
                            id = item.ItemId ?? 0,
                            quantity = item.ChangeOrderQuantity ?? 0,
                            missing = (itemsQuantities.FirstOrDefault(q => q.Id == item.ItemId).Quantity -
                            item.Extra ?? 1)
                        });
                    }
                }
            }

            //create new list with the sale details
            //the fields: categoryName, categoryId, categoryOrder, itemId, itemName, quantity, extra, unitsInPackage.
            var saleSummary = _context.TfamilyItemSale.Include(x => x.SaleItem).Include(x => x.SaleItem.Item).Where(s => s.SaleItem.SaleId == saleId && s.SaleItem.Item.ParentLinkId == null).Select(saleItem =>
            //ThenInclude(x=>x.Item)     
            new
            {
                          categoryName = saleItem.SaleItem.Item.Category.CategoryName,
                          categoryId = saleItem.SaleItem.Item.CategoryId,
                          categoryOrder = saleItem.SaleItem.Item.Category.OrderInSys,
                          itemId = saleItem.SaleItem.ItemId,
                          itemName = saleItem.SaleItem.Item.Name,
                          quantity = itemsQuantities.FirstOrDefault(i => i.Id == saleItem.SaleItem.ItemId).Quantity,
                          extraFromPreviousSale = saleItem.SaleItem.Extra,
                          packagesToOrder = packagesToOrder.FirstOrDefault(p => p.id == saleItem.SaleItem.ItemId) != null ? packagesToOrder.FirstOrDefault(p => p.id == saleItem.SaleItem.ItemId).quantity : 0,
                          extra = 0,
                          missing = packagesToOrder.FirstOrDefault(p => p.id == saleItem.SaleItem.ItemId) != null ? packagesToOrder.FirstOrDefault(p => p.id == saleItem.SaleItem.ItemId).missing : 0,
                          unitsInPackage = saleItem.SaleItem.Item.UnitsInPackage,
                          isCome = (saleItem.SaleItem.IsCome!=null)?true:false
                      }
            ).ToList();
           
            return Ok(saleSummary);
        }

        // PUT: api/SaleSummary/5
        [HttpPut, Route("ChangePackagesToOrder/{id}")]
        [Authorize]
        public async Task<IActionResult> ChangePackagesToOrder(int id, [FromBody] SaleSummaryItem saleSummaryItem)
        {
            var saleItem =_context.TsaleItem.LastOrDefault(s => s.ItemId  == id );//todo +saleId
            saleItem.ChangeOrderQuantity = saleSummaryItem.ChangeOrderQuantity;
            saleItem.Extra = saleSummaryItem.Extra;
            saleItem.IsCome = saleSummaryItem.IsCome;
            int saleId = saleItem.SaleId.Value ;
            _context.SaveChanges();
            return Ok(GetTsaleSummary(saleId));
        }

        // POST: api/SaleSummary
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostTsaleItem([FromBody] TsaleItem tsaleItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.TsaleItem.Add(tsaleItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTsaleItem", new { id = tsaleItem.SaleItemId }, tsaleItem);
        }

        // DELETE: api/SaleSummary/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteTsaleItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tsaleItem = await _context.TsaleItem.FindAsync(id);
            if (tsaleItem == null)
            {
                return NotFound();
            }

            _context.TsaleItem.Remove(tsaleItem);
            await _context.SaveChangesAsync();

            return Ok(tsaleItem);
        }

        [Authorize]
        private bool TsaleItemExists(int id)
        {
            return _context.TsaleItem.Any(e => e.SaleItemId == id);
        }
    }
}