﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Entities;
using WebChicken.Helpers;
using WebChicken.Models;
using WebChicken.Entities.Enum;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace WebChicken.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FamilySalesController : ControllerBase
    {

        private readonly ILog logger;
        private ChickenSaleContext _context;

        private readonly ILog _logger;

          private  List<FamilyExcelItem> familyExcelItems;

        public FamilySalesController(ChickenSaleContext context, ILog logger)
        {
            this.logger = logger;
            _context = context;
            _logger = logger;
        }

        //[HttpGet]
        //public IEnumerable<TfamilySale> GetTfamilySale()
        //{
        //    return _context.TfamilySale;
        //}

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTfamilySale([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfamilySale = await _context.TfamilySale.FindAsync(id);

            if (tfamilySale == null)
            {
                return NotFound();
            }

            return Ok(tfamilySale);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTfamilySale([FromRoute] int id, [FromBody] TfamilySale tfamilySale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tfamilySale.FamilySaleId)
            {
                return BadRequest();
            }

            _context.Entry(tfamilySale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TfamilySaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        //[HttpPost]
        //public async Task<IActionResult> PostTfamilySale([FromBody] TfamilySale tfamilySale)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    _context.TfamilySale.Add(tfamilySale);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetTfamilySale", new { id = tfamilySale.FamilySaleId }, tfamilySale);
        //}

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTfamilySale([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfamilySale = await _context.TfamilySale.FindAsync(id);
            if (tfamilySale == null)
            {
                return NotFound();
            }

            _context.TfamilySale.Remove(tfamilySale);
            await _context.SaveChangesAsync();

            return Ok(tfamilySale);
        }

        private bool TfamilySaleExists(int id)
        {
            return _context.TfamilySale.Any(e => e.FamilySaleId == id);
        }

        /// <summary>
        /// עדכון כמויות למכירה
        /// </summary>
        [HttpPost,Route ("UploadFiles")]
        public  void  AddSaleAsync(IFormFile excelfile)
        {

            var fn = excelfile.FileName;
            var time = DateTime.Now.Ticks;
            var filePath = @".\MyFiles\FamiliesExcelItems";

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
             filePath = Path.Combine(filePath,  time + fn);
            try
            {
                
                    if (excelfile.Length > 0)
                    {
                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                        excelfile.CopyTo(stream);
                        }
                    }
                
            }
            catch(Exception e)
            {
                logger.Error("error in FamilySales controller in AddSaleFiles function " , e);
            }
            try
            { 
            //קבלת הליסטים
             familyExcelItems =
                 ExcelParser<FamilyExcelItem>.ReadDocument(filePath, FamilyExcelItem.getColumnsNames());
            }
            catch (Exception e)
            {
                logger.Error("error in FamilySales controller in AddSaleFiles function ", e);
            }
            try
            {
            //המכירה האחרונה
            Tsale tsale = _context.Tsale.Last(s => s.SysRowStatus == 1);
            //אם המכירה אינה פתוחה להזמנות או שהיא אמורה להמחק
            //תשלח הודעת שגיאה
            if (tsale.StatusId == (int)SaleStatus.OpenForOrders && tsale.SysRowStatus == 1)
            {
                //מעבר על כל הרשומות שבקובץ
                foreach (var sale in familyExcelItems)
                {
                    //אם תאריך ההזמנה מתאים לתאריך המכירה
                    if (sale.LastChange.CompareTo(tsale.CreateDate) >= 0 && sale.LastChange.CompareTo(tsale.FinishDate) <= 0)
                    {
                        //אם המשפחה אינה קיימת במאגר
                        //מוסיפים את המשפחה
                        if (_context.Tfamily.FirstOrDefault(f => f.FamilyIdPhone == sale.FamilyId) == null)
                        {
                            //חילוץ הטלפונים של המשפחה
                            string[] phone = sale.PhonesNumbers.Split(',');
                            //יצירת אובייקט חדש של משפחה והכנסתו לטבלה
                            _context.Tfamily.Add(new Tfamily() { Phone = phone[0], Phone2 = (phone.Length > 1) ? phone[1] : null,Name=sale.FullName, Mail = null, IsActive = null, IsOnlyMail = null, Address = null, FamilyIdPhone = sale.FamilyId, CreatedByUserId = Convert.ToInt32(User.Identity.Name), CreateDate = DateTime.Now, LastModifyUserId = null, LastModifyDate = null, SysRowStatus = 1 });
                            //שמירת שינויים
                            _context.SaveChanges();
                        }
                        // אם המשפחה כבר קיימת או אחרי שהוסיפו את המשפחה
                        //שליפת קוד המשפחה
                        int familyId = _context.Tfamily.FirstOrDefault(f => f.FamilyIdPhone == sale.FamilyId).FamilyId;

                        TsaleItem tsaleItem= _context.TsaleItem.Where(si => si.SaleId == tsale.SaleId).FirstOrDefault(si => si.ItemId == sale.ItemId && si.SysRowStatus == 1);

                        //אם המוצר קיים במכירה הנוכחית
                        if (tsaleItem != null)
                        {
                            //אם אין הזמנה פתוחה למשפחה
                            if(_context.TfamilySale.FirstOrDefault(fs=>fs.FamilyId == familyId && fs.SaleId == tsale.SaleId && fs.SysRowStatus ==1)==null)
                            {
                                //פתיחת הזמנה למשפחה
                                TfamilySale tfamilySale = new TfamilySale() { FamilyId = familyId, SaleId = tsale.SaleId, CreatedByUserId = Convert.ToInt32(User.Identity.Name), CreateDate= DateTime.Now, SysRowStatus= 1 };
                                _context.TfamilySale.Add(tfamilySale);
                                _context.SaveChanges();
                            }

                            //כשיש הזמנה למשפחה
                            //שליפת הקוד של ההזמנה
                            int familySaleId = _context.TfamilySale.First(fs => fs.FamilyId ==familyId && fs.SaleId == tsale.SaleId && fs.SysRowStatus == 1).FamilySaleId;
                            TsaleItem item = _context.TsaleItem.LastOrDefault(i => i.ItemId == sale.ItemId); 
                            //נסיון לשליפת הפריט מההזמנה
                            TfamilyItemSale tfamilyItemSale= _context.TfamilyItemSale.FirstOrDefault(si => si.FamilySaleId == familySaleId && si.SaleItemId == item.SaleItemId);
                            //לבדוק האם קיים הפריט בהזמנה
                            if (tfamilyItemSale != null)
                            {
                                //אם כן לבדוק תאריך האם לעדכן או לא
                                if (sale.LastChange.CompareTo(DateTime.Now) <= 0)
                                //צריך לעדכן את הפריט
                                {
                                    //שינוי הכמות באוביקט
                                    tfamilyItemSale.Quantity = sale.NumberOfUnits;
                                        tfamilyItemSale.LastModifyDate = DateTime.Now;
                                        tfamilyItemSale.LastModifyUserId = Convert.ToInt32(User.Identity.Name);
                                    //עדכון במסד הנתונים
                                    _context.Entry(tfamilyItemSale).State = EntityState.Modified;
                                    _context.SaveChanges();
                                }

                            }
                            //אם לא קיים הפריט בהזמנה
                            else
                            {
                                //הוספת הפריט להזמנה
                                TfamilyItemSale itemSale =
                                    new TfamilyItemSale() { FamilySaleId = familySaleId, SaleItemId = tsaleItem.SaleItemId, Quantity = sale.NumberOfUnits, CreateDate= DateTime.Now, CreatedByUserId = Convert.ToInt32(User.Identity.Name), SysRowStatus = 1 ,OrderWay=(int)OrderWay.Phone };
                                _context.TfamilyItemSale.Add(itemSale);
                                _context.SaveChanges();
                            }

                        }
                        //אם המוצר אינו קיים במכירה הנוכחית שולחים הודעת שגיאה
                        else
                        {
                            throw new ItemNotExistsInSaleException("הפריט אינו קיים במכירה הנוכחית, אנא בדוק את הזמנתך שוב");
                        }
                    }

                    //אם התאריך אינו תואם נזרקת הודעה
                    else
                    {
                        throw new SaleTimeNotCorrectException("זמן ההזמנה אינו תואם לזמן המכירה");
                    }
                }

            }
            //אם המכירה אינה פתוחה להזמנות או שנמחקה
            else
                throw new SaleNotOpenToOrderException("המכירה אינה פתוחה להזמנות");
            }
            catch (Exception e)
            {
                logger.Error("error in FamilySales controller in AddSaleFiles function ", e);
            }


        }

        private string SendMessage_quantityMore30(List<ExcelFamilySale> listItem_inValid)
        {
            string content = "";
            string itemsList = "";
            foreach (var item_inValid in listItem_inValid)
            {
                itemsList += "<br/>" + item_inValid.NameItem + "<br/>";
            }
            content = _context.TemailTemplates.First(x => x.Name == "MoreThanThirtyUnits").Value;
            content = content.Replace("@items", itemsList);
            return content;
        }
        private string SendMessage_TheProductDidntKeep()
        {
            return "עקב שגיאה במערכת המוצרים לא נקלטו בהזמה";
        }
        /// <summary>
        /// פונקציה המקבלת מייל של משפחה ומבצעת הוספה או עדכון של הזמנה
        /// </summary>
        /// <param name="addressEmail"></param>
        /// <returns>מחזירה מייל לפי </returns>
        [AllowAnonymous]
        [HttpGet ,Route("GetOrderDataToDB/{addressEmail}/{pathFile}")]
        public async Task<IActionResult> GetOrderDataToDB(string addressEmail, string pathFile)
        {
            string message = "";
            string[] to = new string[1];
            to[0] = addressEmail;
            int idSale;
            //אם קיימת מכירה פתוחה

            //int.Parse(SaleStatus.OpenForOrders.ToString())
            //אני אוחזת פה זה נתקע בהמרה של האינם
          //  if (_context.Tsale.FirstOrDefault(s => s.StatusId==5) != null)

            if (_context.Tsale.FirstOrDefault(s => s.StatusId == (int)SaleStatus.OpenForOrders) != null)
            {
                idSale = _context.Tsale.FirstOrDefault(s => s.StatusId == (int)SaleStatus.OpenForOrders && s.SysRowStatus==1).SaleId;
            }
            else
            {
                //אם המכירה לא פתוחה להזמנה ישלח מייל חזרה למשפחה 
                // "המכירה לא פתוחה להזמנות";  
                message = _context.TemailTemplates.First(m => m.Name == "closeSale").Value;
                return Ok(Messeage.SendEmail(to, "", message));
            }
            //אם קיימת משפחה שיש לה את המייל הזה
            int idFamily;
            if (_context.Tfamily.FirstOrDefault(f => f.Mail == addressEmail) != null)
            {
                idFamily = _context.Tfamily.FirstOrDefault(f => f.Mail == addressEmail).FamilyId;
            }
            else
            {
                //אם לא קיימת משפחה שולח מייל חזרה שלא נמצאה רשומה

                message = _context.TemailTemplates.First(m => m.Name == "noRecordFound").Value;
                return Ok(Messeage.SendEmail(to, "", message));
            }
            //שליחת הקובץ לקריאה
            //וקבלתו מפורק לליסט שבכל איבר יש מוצר שהוזמן
            List<ExcelFamilySale> listItemToOrder;
            try
            {
                listItemToOrder = ExcelParser<ExcelFamilySale>.ReadDocument("C:/WebChicken/ReadEmail/filesFromEmail/" + pathFile, ExcelFamilySale.GetColumnsName());
            }
            catch
            {
                message = _context.TemailTemplates.First(m => m.Name == "ExcelFileProblem").Value;
                return Ok(Messeage.SendEmail(to, "", message));
            }
            //במקרה של הזמנה, לצורך בטיחות, נגביל את הכמות שהוזמנה,
            //הערך נמצא בטבלת גלובל, נוציא אותו משם. 
            //במקרה ולא יהיה קיים נוסיף מפה את הערך
            int MaxQuantity = 30;
            if (_context.Tglobal.FirstOrDefault(g => g.Name == "MaxQuantity").Value != null)
                MaxQuantity = int.Parse(_context.Tglobal.FirstOrDefault(g => g.Name == "MaxQuantity").Value.ToString());
            //רשימה לשמירת הפריטים שלא נקלטו או עודכנו בהזמנה מכייון שהכמות שלהם עולה על שלושים יחדות
            List<ExcelFamilySale> listItem_inValid = new List<ExcelFamilySale>();

            //בודק אם קיימת רשומה שהמשפחה הזמינה כבר למכירה זו
            //אם קיימת סימן שזה עדכון
            //אחרת זה הוספה

            //משתנה דגל ששומר אם זה עדכון הזמנה או הוספה- כדי לדעת בסוף התהליך איזה הודעה לשלוח
            bool isEditOrder = false;
            //של הרשומה שכולל את המשפחה ומספר הזמנה id משתנה לשמירת ה 
            int familySale;
            if (_context.TfamilySale.FirstOrDefault(f => f.FamilyId == idFamily && f.SaleId == idSale) != null)
            {
                //נמצאה הרשומה- המשפחה הזמינה כבר במכירה זו ולכן זה עדכון
                familySale = _context.TfamilySale.FirstOrDefault(f => f.FamilyId == idFamily && f.SaleId == idSale).FamilySaleId;
                //מביוון שזה עדכון: סימון הדגל כעריכה- 1
                isEditOrder = true;
                //בדיקה אם המשפחה הזמינה בקובץ אקסל
                //אם ההזמנה נערכה בקובץ אקסל- ניתן לעדכן
                //אחרת לא ניתן לעדכן- נשלחת הודעת מייל מתאימה שההזמנה לא עודכנה

                if (_context.TfamilyItemSale.FirstOrDefault(f => f.FamilySaleId == familySale) == null || _context.TfamilyItemSale.First(f => f.FamilySaleId == familySale).OrderWay == (int)OrderWay.Excel)
                {   //ראשית בדיקה אם קיימים בכלל מוצרים לאותה משפחה
                    foreach (ExcelFamilySale item in listItemToOrder)
                    {
                        //(בדיקה אם הכמות לא מוגזמת מידי (מקסימום 30
                        if (item.Quantity < MaxQuantity)
                        {
                            //חיפוש הפריט בטבלת הזמנות
                         
                            if (_context.TfamilyItemSale.FirstOrDefault(f => f.FamilySaleId == familySale && f.SaleItemId == item.CodMenuPhone) != null)
                            { //הפריט נמצא- עדכון כמות
                                _context.TfamilyItemSale.First(f => f.FamilySaleId == familySale && f.SaleItemId == item.CodMenuPhone).Quantity = item.Quantity;
                            }
                            else
                            {
                                //הפריט לא נמצא - הוספת הפריט לטבלה
                                _context.TfamilyItemSale.Add(new TfamilyItemSale()
                                {
                                    FamilySaleId = familySale,
                                    SaleItemId = item.CodMenuPhone,
                                    Quantity = item.Quantity,
                                    OrderWay = (int)OrderWay.Excel,
                                    CreateDate = DateTime.Now,
                                    CreatedByUserId = 2,
                                    SysRowStatus = 1

                                });
                            }
                        }
                        else
                        {
                            //הכמות מוגזמת מידי
                            //הוספת הפריט לרשימה
                            listItem_inValid.Add(item);
                        }
                    }
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch(Exception ex)
                    {
                        _logger.Error("The product didnt keep in data-base", ex);
                        return Ok(Messeage.SendEmail(to, "", SendMessage_TheProductDidntKeep()));
                        //////https://docs.microsoft.com/en-us/dotnet/api/system.servicemodel.description.serviceendpoint?view=netframework-4.8
                        //מה עושים??????????????????????????
                    }
                }
                else
                {
                    //ההזמנה לא בוצעה באקסל נשלחת הודעה מתאימה
                    message = _context.TemailTemplates.First(m => m.Name == "TheOrderWasNotPlacedInExcel").Value;
                    return Ok(Messeage.SendEmail(to, "", message, true));
                }
            }
            else
            {
                //לא נמצאה הזמנה למשפחה במכירה זו
                //ביצוע הזמנה:
                //TfamilySale הוספת רשומה בטבלת 
                //TfamilyItemSale הוספת הפריטים בטבלת 
                _context.TfamilySale.Add(new TfamilySale()
                {
                    FamilyId = idFamily,
                    SaleId = idSale,
                    CreatedByUserId = 2,
                    CreateDate = DateTime.Now,
                    SysRowStatus = 1
                });
                try
                {
                    _context.SaveChanges();
                    //TfamilySale של הרשומה החדשה מטבלת id שמירת ה
                    familySale = _context.TfamilySale.First(fs => fs.FamilyId == idFamily && fs.SaleId == idSale).FamilySaleId;
                    //הוספת הפריטים של ההזמנה
                    foreach (ExcelFamilySale item in listItemToOrder)
                    {
                        //(בדיקה אם הכמות לא מוגזמת מידי (מקסימום 30
                        if (item.Quantity < MaxQuantity)
                        {
                            _context.TfamilyItemSale.Add(new TfamilyItemSale()
                            {
                                FamilySaleId = (int)familySale,
                                SaleItemId = (int)item.CodMenuPhone,
                                Quantity = (int)item.Quantity,
                                OrderWay = (int)OrderWay.Excel,
                                CreateDate = DateTime.Now,
                                CreatedByUserId = 2,
                                SysRowStatus = 1
                            });
                        }
                        else
                        {
                            //הכמות מוגזמת מידי
                            //הוספת הפריט לרשימה
                            listItem_inValid.Add(item);
                        }
                    }
                     _context.SaveChanges();

                }
                catch(Exception ex)
                {
                    _logger.Error("The product or family didnt keep in data-base", ex);
                    return Ok(Messeage.SendEmail(to, "", SendMessage_TheProductDidntKeep()));
                }
            }
            //שליחת מייל כאישור לביצוע או עדכון ההזמנה
            if(isEditOrder)//עריכת הזמנה
            {
                message = _context.TemailTemplates.First(m => m.Name == "orderUpdated").Value;
            }
            else
            {
                //הוספת הזמנה
                message = _context.TemailTemplates.First(m => m.Name == "orderWasPlaced").Value;
            }
            //אם נמצאו רשומות ברשימה של הפריטים הלא חוקיים נשלחת ללקוח הודעה מתאימה
            if (listItem_inValid.Count > 0)
                message += "<br/><br/>שים לב:<br/>" + SendMessage_quantityMore30(listItem_inValid);
            return Ok(Messeage.SendEmail(to, "", message, true));



            //}
        }
    }

    //1.בדיקה שיש מכירה בסטטוס פתוח להזמנות אחרת שולח מייל מתאים.

    //new List<ExcelFamilySale>();
    //2. בדיקה אם מייל קיים במשפחות, 
    //יש לקלוט את ההזמנה אחרת שולח מייל :משתמש לא רשום 
    //בקוד יש לשמור קוד מכירה וקוד משפחה לפי מייל.

    //3.יש לבדוק בטבלת familysale  אם קיימת רשומה למכירה נוכחית
    //לפי קוד מכירה וקוד משפחה 
    //אחרת יש להוסיף.
}