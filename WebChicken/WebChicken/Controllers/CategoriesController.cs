﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Helpers;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/categories")]
    [ApiController]
    [Authorize(Roles = RolesNames.ADMIN)]
    public class CategoriesController : ControllerBase
    {
        private readonly ChickenSaleContext _context;

        public CategoriesController(ChickenSaleContext context)
        {
            _context = context;
        }

        // GET: api/Categories
        [HttpGet]
        public IEnumerable<Tcategories> GetTcategories()
        {
            return _context.Tcategories;
        }

        // GET: api/Categories/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTcategories([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tcategories = await _context.Tcategories.FindAsync(id);

            if (tcategories == null)
            {
                return NotFound();
            }

            return Ok(tcategories);
        }

        // PUT: api/Categories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTcategories([FromRoute] int id, [FromBody] Tcategories tcategories)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tcategories.CategoryId)
            {
                return BadRequest();
            }

            _context.Entry(tcategories).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TcategoriesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Categories
        [HttpPost]
        public async Task<IActionResult> PostTcategories([FromBody] Tcategories tcategories)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tcategories.Add(tcategories);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTcategories", new { id = tcategories.CategoryId }, tcategories);
        }

        // DELETE: api/Categories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTcategories([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tcategories = await _context.Tcategories.FindAsync(id);
            if (tcategories == null)
            {
                return NotFound();
            }

            _context.Tcategories.Remove(tcategories);
            await _context.SaveChangesAsync();

            return Ok(tcategories);
        }

        private bool TcategoriesExists(int id)
        {
            return _context.Tcategories.Any(e => e.CategoryId == id);
        }
    }
}