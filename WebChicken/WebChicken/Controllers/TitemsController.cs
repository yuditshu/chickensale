﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TitemsController : ControllerBase
    {
        private readonly ChickenSaleContext _context;

        public TitemsController(ChickenSaleContext context)
        {
            _context = context;
        }

        // GET: api/Titems
        [HttpGet]
        public IEnumerable<Titem> GetTitem()
        {
            return _context.Titem;
        }


        // GET: api/Titems/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTitem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var titem = await _context.Titem.FindAsync(id);

            if (titem == null)
            {
                return NotFound();
            }

            return Ok(titem);
        }

        // PUT: api/Titems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTitem([FromRoute] int id, [FromBody] Titem titem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != titem.ItemId)
            {
                return BadRequest();
            }

            _context.Entry(titem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TitemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Titems
        [HttpPost]
        public async Task<IActionResult> PostTitem([FromBody] Titem titem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Titem.Add(titem);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TitemExists(titem.ItemId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTitem", new { id = titem.ItemId }, titem);
        }

        // DELETE: api/Titems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTitem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var titem = await _context.Titem.FindAsync(id);
            if (titem == null)
            {
                return NotFound();
            }

            _context.Titem.Remove(titem);
            await _context.SaveChangesAsync();

            return Ok(titem);
        }

        private bool TitemExists(int id)
        {
            return _context.Titem.Any(e => e.ItemId == id);
        }
    }
}