﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Entities;
using WebChicken.Entities.Enum;
using WebChicken.Helpers;
using WebChicken.Models;
using OfficeOpenXml;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace WebChicken.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SaleItemsController : ControllerBase
    {
        private readonly ChickenSaleContext _context;

        private readonly ILog _logger;

        public SaleItemsController(ChickenSaleContext context, ILog logger)
        {
            _context = context;
            _logger = logger;
        }





        // GET: api/SaleItems
        [HttpGet]
        public IEnumerable<object> GetTsaleItem()
        {
            List<object> li = new List<object>();

            foreach (var ns in _context.TsaleItem)
            {
                if (ns.SysRowStatus != 0)
                {
                    li.Add(new
                    {
                        SaleItemId = ns.SaleItemId,
                        SaleId = ns.SaleId,
                        ItemId = ns.ItemId,
                        IsCome = ConvertBooleanToVX.Convert(ns.IsCome),
                        FormRemark = ns.FormRemark,
                        SignRemark = ns.SignRemark,
                        IfPrintSign = ConvertBooleanToVX.Convert(ns.IfPrintSign),
                        Extra = ns.Extra,
                        ChangeOrderQuantity = ns.ChangeOrderQuantity,

                        ns.CreateDate,
                        SysRowStatus = ns.SysRowStatus,
                        CreatedByUser = ns.CreatedByUserId,
                        LastModifyUser = ns.LastModifyUserId
                    });
                }
            }
            return li;
        }

        // GET: api/SaleItems/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTsaleItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TsaleItem tsaleItem = await _context.TsaleItem.FindAsync(id);



            if (tsaleItem == null)
            {
                return NotFound();
            }
            var ConvertedTsaleItem = new
            {
                SaleItemId = tsaleItem.SaleItemId,
                SaleId = tsaleItem.SaleId,
                ItemId = tsaleItem.ItemId,
                IsCome = ConvertBooleanToVX.Convert(tsaleItem.IsCome),
                FormRemark = tsaleItem.FormRemark,
                SignRemark = tsaleItem.SignRemark,
                IfPrintSign = ConvertBooleanToVX.Convert(tsaleItem.IfPrintSign),
                Extra = tsaleItem.Extra,
                ChangeOrderQuantity = tsaleItem.ChangeOrderQuantity,

                tsaleItem.CreateDate,
                SysRowStatus = tsaleItem.SysRowStatus,
                CreatedByUser = tsaleItem.CreatedByUserId,
                LastModifyUser = tsaleItem.LastModifyUserId
            };

            return Ok(ConvertedTsaleItem);
        }

        // GET: api/SaleItemsByIdSale/5
        [HttpGet, Route("SaleItemsByIdSale/{id}")]
        public async Task<IActionResult> GetTsaleItemByIdSale([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<object> li = new List<object>();

            foreach (var ns in _context.TsaleItem.Where(s => s.SysRowStatus != 0))
            {
                if (ns.SaleId == id)
                {
                    li.Add(new
                    {
                        SaleItemId = ns.SaleItemId,
                        SaleId = ns.SaleId,
                        ItemId = ns.ItemId,
                        IsCome = ConvertBooleanToVX.Convert(ns.IsCome),
                        FormRemark = ns.FormRemark,
                        SignRemark = ns.SignRemark,
                        IfPrintSign = ConvertBooleanToVX.Convert(ns.IfPrintSign),
                        Extra = ns.Extra,
                        ChangeOrderQuantity = ns.ChangeOrderQuantity,

                        ns.CreateDate,
                        SysRowStatus = ns.SysRowStatus,
                        CreatedByUser = ns.CreatedByUserId,
                        LastModifyUser = ns.LastModifyUserId
                    });
                }
            }
            return Ok(li);
        }

        // PUT: api/SaleItems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTsaleItem([FromRoute] int id, [FromBody] TsaleItem tsaleItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tsaleItem.SaleItemId)
            {
                return BadRequest();
            }

            _context.Entry(tsaleItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TsaleItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SaleItems
        [HttpPost]
        public async Task<IActionResult> PostTsaleItem([FromBody] TsaleItem tsaleItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.TsaleItem.Add(tsaleItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTsaleItem", new { id = tsaleItem.SaleItemId }, tsaleItem);
        }

        // DELETE: api/SaleItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTsaleItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tsaleItem = await _context.TsaleItem.FindAsync(id);
            if (tsaleItem == null)
            {
                return NotFound();
            }

            tsaleItem.SysRowStatus = 0;
            await _context.SaveChangesAsync();

            return Ok(tsaleItem);
        }

        private bool TsaleItemExists(int id)
        {
            return _context.TsaleItem.Any(e => e.SaleItemId == id);
        }

        [HttpPost, Route("AddItemsForSale")]
        //[Authorize(Roles = "Admin")]
        //פונקציה זו תקבל את הרשימה שנוצרה מקובץ אקסל לפריטים למכירה
        public IActionResult AddItemsForSale(IFormFile excelFile)
        {
            if (excelFile == null || excelFile.Length == 0)
                throw new Exception ("Please select profile picture");

            var folderName = Path.Combine("Resources", "excel");
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), folderName);

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            var uniqueFileName = $"{excelFile.FileName }";
            var dbPath = Path.Combine(folderName, uniqueFileName);

            using (var fileStream = new FileStream(Path.Combine(filePath, uniqueFileName), FileMode.Create))
            {
                 excelFile.CopyTo(fileStream);
            }
            //var request = HttpContext.Current.Request;
            //var filePath = "C:\\temp\\" + request.Headers["filename"];
            //using (var fs = new System.IO.FileStream(filePath, System.IO.FileMode.Create))
            //{
            //    request.InputStream.CopyTo(fs);
            //}


            //HttpResponseMessage result = null;
            //var httpRequest = HttpContext.Current.Request;
            //if (httpRequest.Files.Count > 0)
            //{
            //    var docfiles = new List<string>();
            //    foreach (string file in httpRequest.Files)
            //    {
            //        var postedFile = httpRequest.Files[file];
            //        var filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);
            //        postedFile.SaveAs(filePath);
            //        docfiles.Add(filePath);
            //    }
            //    result = Request.CreateResponse(HttpStatusCode.Created, docfiles);
            //}
            //else
            //{
            //    result = Request.CreateResponse(HttpStatusCode.BadRequest);
            //}
            //return result;


            //// full path to file in temp location
            //var filePath = Path.GetTempFileName();


            //    if (excelFile.Length > 0)
            //    {
            //        using (var stream = new FileStream(filePath, FileMode.Create))
            //        {
            //          //  await excelFile.CopyToAsync(stream);
            //        }
            //    }

            //קריאת הרשומות מקובץ האקסל

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("StationId", "קוד תחנה");
            dic.Add("CategoryId", "קוד קטגוריה");
            dic.Add("OrderCategories", "סדר בטלפון לקטגוריה");
            dic.Add("CategoryName", "שם קטגוריה");
            dic.Add("ItemId", "קוד מוצר");
            dic.Add("ItemName", "שם מוצר");
            dic.Add("Remark", "הערות");
            dic.Add("PrintRemark", "הערות להדפסה");
            dic.Add("OrderItems", "סדר בטלפון");
            dic.Add("Unit", "יחידה");
            dic.Add("Price", "מחיר");
            dic.Add("IsWeight", "לפי משקל");
        

            List<ExcelSaleItem> Items = ExcelParser<ExcelSaleItem>.ReadDocument(dbPath, dic);
            int? saleId;
            //קיים מכירה פתוחה-אם המכירה פתוחה
            if ((saleId = _context.Tsale.FirstOrDefault(x => x.SysRowStatus == 1 && x.Status.StatusId == Convert.ToInt32(SaleStatus.Opened))?.SaleId) != null)
            {
                //אם קיימים פריטים למכירה
                //if (_context.TsaleItem.Count(x => x.SaleId == saleId) > 0)//:todo
                //return BadRequest(".התווספו כבר פריטים למכירה,ניתן לערוך ידנית");
                foreach (ExcelSaleItem item in Items)
                {
                    //אם הקטגוריה לא קיימת יצירת קטגוריה חדשה
                    if (_context.Tcategories.Count(x => x.CategoryId == item.CategoryId) <= 0)
                    {
                        Tcategories c = new Tcategories()
                        {
                            CategoryId = item.CategoryId,
                            CategoryName = item.CategoryName,
                            OrderInSys = item.OrderCategories,
                            CreateDate = DateTime.Now,
                            CreatedByUserId = Convert.ToInt32(User.Identity.Name),
                            SysRowStatus = 1,
                        };
                        _context.Tcategories.Add(c);
                        try
                        {
                            _context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("AddItemsForSale", ex);
                            return BadRequest("הנתונים לא נשמרו במערכת...");
                        }

                    }


                    //אם הפריט לא קיים יצירת פריט חדש
                    if (_context.Titem.Count(x => x.SysRowStatus == 1 && x.ItemId == item.ItemId) <= 0)
                    {
                        //אם היחידה המבוקשת לא קיימת יצירה 
                        Tunits myUnit = _context.Tunits.FirstOrDefault(a => a.Singular == item.Unit);
                        int unitId;
                        if (myUnit != null)
                        {
                            unitId = myUnit.UnitId;
                        }
                        else
                        {
                            Tunits u = new Tunits()
                            {
                                Singular = item.Unit,
                                IsWeight = item.IsWeight,
                                Plural = item.Unit + "ים",
                                CreateDate = DateTime.Now,
                                CreatedByUserId = Convert.ToInt32(User.Identity.Name),
                                SysRowStatus = 1


                            };
                            _context.Tunits.Add(u);
                            try
                            {
                                _context.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                _logger.Error("AddItemsForSale", ex);
                                return BadRequest("הנתונים לא נשמרו במערכת...");
                            }
                            unitId = u.UnitId;
                        }
                        Titem t = new Titem()
                        {
                            ItemId = item.ItemId,
                            Name = item.ItemName,
                            CategoryId = item.CategoryId,
                            OrderItems = item.OrderItems,
                            OrderCategories = item.OrderCategories,
                            Price = item.Price,
                            UnitId = unitId,
                            CreateDate = DateTime.Now,
                            CreatedByUserId = Convert.ToInt32(User.Identity.Name),
                            SysRowStatus = 1,
                        };
                        _context.Titem.Add(t);
                        try
                        {
                            _context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("AddItemsForSale", ex);
                            return BadRequest("הנתונים לא נשמרו במערכת...");
                        }
                    }
                    //אם המוצר קיים במערכת - עדכון
                    if (_context.TsaleItem.Where(x=>x.SaleId== saleId).Count(a => a.SysRowStatus == 1 && a.ItemId == item.ItemId) > 0)
                    {
                        TsaleItem itemToUpdate = _context.TsaleItem.FirstOrDefault(a => a.ItemId == item.ItemId && a.SaleId == saleId);
                        if (itemToUpdate != null)
                        {
                            itemToUpdate.ItemId = item.ItemId;
                            itemToUpdate.FormRemark = item.Remark;
                            itemToUpdate.SignRemark = item.PrintRemark;
                            itemToUpdate.Extra = 0;
                            itemToUpdate.CreatedByUserId = Convert.ToInt32(User.Identity.Name);
                            itemToUpdate.SysRowStatus = 1;
                            itemToUpdate.LastModifyDate = DateTime.Now;
                            itemToUpdate.LastModifyUserId = Convert.ToInt32(User.Identity.Name);
                        }
                    }
                    else
                    {
                        TsaleItem t = new TsaleItem()
                        {
                            ItemId = item.ItemId,
                            IsCome = null,
                            FormRemark = item.Remark,
                            SignRemark = item.PrintRemark,
                            IfPrintSign = null,
                            Extra = null,
                            ChangeOrderQuantity = null,
                            CreatedByUserId = Convert.ToInt32(User.Identity.Name),
                            SysRowStatus = 1,
                            SaleId = saleId

                        };
                        _context.TsaleItem.Add(t);
                        try
                        {
                            _context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("AddItemsForSale", ex);
                            return BadRequest("הנתונים לא נשמרו במערכת...");
                        }
                    }

                }
                //עדכון סטטוס מכירה נפתח להזמנה
                _context.Tsale.Find(saleId).StatusId = Convert.ToInt32(SaleStatus.OpenForOrders);
                _context.SaveChanges();
                string body = _context.TemailTemplates.FirstOrDefault(x => x.EmailTemplatesId == (int)EmailTemplae.OpenSale).Value;// _context.TEmail;//todo enum
                string attachmentFilename = createExcelFile(Items).ToString();//
                string[] allAtachments = { attachmentFilename };
                string[] familyMails = { "yuditshu@gmail.com" };//todo
                int i = 0;
                //עובר על רשימת המשפחות ושולח להם את הודעת המייל
                foreach (Tfamily family in _context.Tfamily)
                {
                    if (family.IsActive == true)
                        familyMails[i++] = family.Mail;
                }
                //  ומכין אותו לשליחה_context שולף את תוכן המייל מה 

                string date = GetHebrewJewishDateString((DateTime)_context.Tsale.First(x => x.SaleId == saleId).Date);
                string nextSaleDate = GetHebrewJewishDateString((DateTime)_context.Tsale.First(x => x.SaleId == saleId).NextSaleDate);
                string address = _context.Tglobal.FirstOrDefault(x => x.GlobalId == Convert.ToInt32(Global.address)).Value;
                string menHour = _context.Tglobal.FirstOrDefault(x => x.GlobalId == Convert.ToInt32(Global.menHour)).Value;
                string womenHour = _context.Tglobal.FirstOrDefault(x => x.GlobalId == Convert.ToInt32(Global.womenHour)).Value;
                string phone = _context.Tglobal.FirstOrDefault(x => x.GlobalId == Convert.ToInt32(Global.phone)).Value;
                string remark1 = _context.Tglobal.FirstOrDefault(x => x.GlobalId == Convert.ToInt32(Global.remark1)).Value;
                string remark2 = _context.Tglobal.FirstOrDefault(x => x.GlobalId == Convert.ToInt32(Global.remark2)).Value;
                string remark3 = _context.Tglobal.FirstOrDefault(x => x.GlobalId == Convert.ToInt32(Global.remark3)).Value;
                string remark4 = _context.Tglobal.FirstOrDefault(x => x.GlobalId == Convert.ToInt32(Global.remark4)).Value;
                string remark5 = _context.Tglobal.FirstOrDefault(x => x.GlobalId == Convert.ToInt32(Global.remark5)).Value;
                string end = _context.Tglobal.FirstOrDefault(x => x.GlobalId == Convert.ToInt32(Global.end)).Value;
                if (date != null) body = body.Replace("@date", date); else body = body.Replace("@date", "");
                if (nextSaleDate != null) body = body.Replace("@nextSale", date); else body = body.Replace("@nextSale", "");
                if (address != null) body = body.Replace("@address", address + "."); else body = body.Replace("@address", "");
                if (menHour != null) body = body.Replace("@menHour", menHour); else body = body.Replace("@menHour", "");
                if (womenHour != null) body = body.Replace("@womenHour", womenHour); else body = body.Replace("@womenHour", "");
                if (phone != null) body = body.Replace("@phone", phone); else body = body.Replace("@phone", "");
                if (remark1 != null) body = body.Replace("@remark1", remark1 + "<br/>"); else body = body.Replace("@remark1", "");
                if (remark2 != null) body = body.Replace("@remark2", remark2 + "<br/>"); else body = body.Replace("@remark2", "");
                if (remark3 != null) body = body.Replace("@remark3", remark3 + "<br/>"); else body = body.Replace("@remark3", "");
                if (remark4 != null) body = body.Replace("@remark4", remark4 + "<br/>"); else body = body.Replace("@remark4", "");
                if (remark5 != null) body = body.Replace("@remark5", remark5 + "<br/>"); else body = body.Replace("@remark5", "");
                if (end != null) body = body.Replace("@end", end); else body = body.Replace("@end", "");
                try
                {
                    Messeage.SendEmail(familyMails, "מכירת העופות והבשר פתוחה להזמנות", body, true, allAtachments);
                }

                catch (Exception ex)
                {
                    _logger.Error("AddItemsForSale", ex);
                    return BadRequest("ההודעות לא נשלחו...");
                }
            }
            else
                return BadRequest("אין מכירה פתוחה");
            return Ok("העדכון בוצעה בהצלחה!!!");
        }
        //מקבל תאריך לועזי ומחזיר אותו בעברי
        public static string GetHebrewJewishDateString(DateTime anyDate)
        {
            System.Text.StringBuilder hebrewFormatedString = new System.Text.StringBuilder();
            // Create the hebrew culture to use hebrew (Jewish) calendar 
            CultureInfo jewishCulture = CultureInfo.CreateSpecificCulture("he-IL");
            jewishCulture.DateTimeFormat.Calendar = new HebrewCalendar();

            // Day of the week in the format " " 
            hebrewFormatedString.Append(anyDate.ToString("dddd", jewishCulture) + " ");
            // Day of the month in the format "'" 
            hebrewFormatedString.Append(anyDate.ToString("dd", jewishCulture) + " ");
            // Month and year in the format " " 
            hebrewFormatedString.Append("" + anyDate.ToString("y", jewishCulture));

            return hebrewFormatedString.ToString();
        }
        [HttpPost, Route("createExcelFile")]
        public FileInfo createExcelFile(List<ExcelSaleItem> Items)
        {

            using (ExcelPackage excel = new ExcelPackage())
            {

                //יצירת הקובץ
                excel.Workbook.Worksheets.Add("Worksheet1");

                FileInfo excelFile = new FileInfo($@".\myFiles\doc{DateTime.Now.Ticks }.xlsx");
                var excelWorksheet = excel.Workbook.Worksheets["Worksheet1"];
                excelWorksheet.View.RightToLeft = true;
                //העמודות
                List<string[]> headerRow = new List<string[]>()
                {
                  new string[] { "קוד מוצר" ,"קוד בתפריט הטלפוני", "שם קטגוריה", "שם מוצר", "הערות", "יחידה","מוצר לפי משקל", "כמות" }
                };
                //המרת העמודות
                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                excelWorksheet.Cells[headerRange].LoadFromArrays(headerRow);
                //עיצוב עמודות הטבלה
                excelWorksheet.Cells[headerRange].Style.Font.Bold = true;
                excelWorksheet.Cells[headerRange].Style.Font.Size = 14;
                excelWorksheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Blue);


                //מילוי ערכי השורות
                var cellData = new List<object[]>();
                using (ChickenSaleContext _context = new ChickenSaleContext())
                {

                    foreach (ExcelSaleItem item in Items)
                    {
                        string mybooltext;
                        if (item.IsWeight == true)
                            mybooltext = "כן";
                        else
                            mybooltext = "לא";

                        cellData.Add(new object[] { item.ItemId, item.OrderItems, item.CategoryName, item.ItemName, item.Remark, item.Unit, mybooltext });
                    }
                }
                excelWorksheet.Cells[2, 1].LoadFromArrays(cellData);

                excel.SaveAs(excelFile);

                return excelFile;
            }
        }


    }
    public class StudentModel
    {
        public string Name { get; set; }
        public IFormFile Image { get; set; }

    }
}