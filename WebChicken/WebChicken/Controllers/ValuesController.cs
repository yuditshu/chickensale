﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebChicken.Entities;
using WebChicken.Helpers;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]//Authorization  Bearer {"UserName":"ST",//"Password":"123456"}
     public class ValuesController : ControllerBase
    {
        private readonly ILog logger;

        public ValuesController(ILog logger)
        {
            this.logger = logger;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<ExcelSaleItem>> Get()
    {
       
            Dictionary<string, string> namesOfColumnsInClassAndFile = new Dictionary<string, string>();
            namesOfColumnsInClassAndFile.Add("StationId", "קוד תחנה");
            namesOfColumnsInClassAndFile.Add("CategoryId", "קוד קטגוריה");
            namesOfColumnsInClassAndFile.Add("PhoneQueueToCategory", "סדר בטלפון לקטגוריה");
            namesOfColumnsInClassAndFile.Add("CategoryName", "שם קטגוריה");
            namesOfColumnsInClassAndFile.Add("ItemId", "קוד מוצר");
            namesOfColumnsInClassAndFile.Add("ItemName", "שם מוצר");
            namesOfColumnsInClassAndFile.Add("Remark", "הערות");
            namesOfColumnsInClassAndFile.Add("PrintRemark", "הערות להדפסה");
            namesOfColumnsInClassAndFile.Add("PhoneQueue", "סדר בטלפון");
            namesOfColumnsInClassAndFile.Add("Unit", "יחידה");
            namesOfColumnsInClassAndFile.Add("Price", "מחיר");
            namesOfColumnsInClassAndFile.Add("IsWeight", "לפי משקל");

             string path = @"Z:\mora\ChikenSale\20190728_Report.xlsx";

            try
            {
               return Ok(ExcelParser<ExcelSaleItem>.ReadDocument(path, namesOfColumnsInClassAndFile));

            }
            catch (NumbersRecordsException e)
            {
                return BadRequest(path + " " + e.Message);
            }
            catch (NumbersColumnsException e)
            {
                return BadRequest(path + " " + e.Message);
            }
            catch (NotCorrectFormatFileException e)
            {
                return BadRequest(path + " " + e.Message);
            }
            catch (Exception e)
            {
                logger.Error("Error is logged",e);
                return BadRequest(e.Message);

            }
          
        }

        private ActionResult BadRequestObjectResult(NumbersRecordsException numbersRecordsException)
        {
            throw new NotImplementedException();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        [HttpPost("UploadFiles")]
        public async Task<IActionResult> Post(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);

            var fn = files[0].FileName;
            var time = DateTime.Now.Ticks;
            var filePath = @".\MyFiles\FamiliesExcelItems\" + time+ fn;
                //Path.GetTempFileName();

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                      await formFile.CopyToAsync(stream);
                    }
                }
            }
            List<FamilyExcelItem> l = new List<FamilyExcelItem>();
            
            return Ok(new { count = files.Count, size, filePath});
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }




    }
}
