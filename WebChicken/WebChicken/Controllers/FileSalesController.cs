﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using Syncfusion.XlsIO;
using WebChicken.Helpers;
using WebChicken.Models;


namespace WebChicken.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FileSalesController : ControllerBase
    {
        private readonly ChickenSaleContext _context;
        //private lo
        [HttpGet]



        public void getCreateFilesExeclProviders(int saleId, int userId)
        {
            //מוצרים הקיימים בהזמנה זו
            var exsistItems = _context.TsaleItem.Where(w => w.SaleId == saleId).ToList();

            List<TfamilyItemSale> itemSales = new List<TfamilyItemSale>();
      

            var quantityToItem = from q in _context.TfamilyItemSale
                                 group q by q.SaleItemId into qq
                                 select new
                                 {
                                     saleItemId = qq.Key,
                                     quantity = qq.Sum(x => x.Quantity),
                                 };

            var itemsInSale = from q in quantityToItem
                              join s in _context.TsaleItem.ToList()
                              on q.saleItemId equals s.SaleItemId
                              where s.SaleId == saleId
                              select new
                              {
                                  saleItemId = s.SaleItemId,
                                  itemId = s.ItemId,
                                  quantity = q.quantity
                              };

            var detailsItem = from a in itemsInSale
                              join i in _context.Titem.ToList()
                              on a.itemId equals i.ItemId
                              where i.ParentLinkId == null
                              orderby i.ProviderId
                              select new
                              {
                                  saleItemId = a.saleItemId,
                                  itemId = a.itemId,
                                  quantity = a.quantity,
                                  provider = i.ProviderId,
                                  nameInFactory = i.NameInFactory,
                                  code = i.BarCode,
                                  description = i.Name,
                                  unitsInPackage = i.UnitsInPackage
                              };


            var TOTALSItemes = from d in itemsInSale
                               join i in _context.Titem.ToList()
                               on d.itemId equals i.ItemId
                               where i.ParentLinkId != null
                               orderby i.ProviderId
                               select new
                               {
                                   saleItemId = d.saleItemId,
                                   itemId = d.itemId,
                                   quantity = d.quantity / 2,
                                   provider = i.ProviderId,
                                   nameInFactory = i.NameInFactory,
                                   code = i.BarCode,
                                   description = i.Name,
                                   unitsInPackage = i.UnitsInPackage,
                                   linkId = i.ParentLinkId
                               };
            var ddd = from d in detailsItem
                      join t in TOTALSItemes
                      on d.itemId equals t.linkId
                      orderby t.provider
                      select new
                      {
                          saleItemId = d.saleItemId,
                          itemId = d.itemId,
                          quantity = d.quantity + t.quantity,
                          provider = d.provider,
                          nameInFactory = d.nameInFactory,
                          code = d.code,
                          description = d.description,
                          unitsInPackage = d.unitsInPackage
                      };

            //foreach (var item in detailsItem)
            //{
            //    if (_context.Titem.FirstOrDefault(f => f.ItemId == item.itemId).ParentLinkId != null)
            //    {
            //        item.quantity = 0;
            //    }
            //}
            var idP = detailsItem.First().provider;


            foreach (var di in detailsItem)
            {
                ExcelWorksheet myExl;
                if (di.provider == idP)
                {
                    TfileSale tfile;
                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        var headerRow = new List<string[]>()
                                    {
                                    new string[] { "תאור הפריט", "קוד", "שם המוצר במפעל", "כמות הזמנה","כמות באריזה" }
                                    };
                        string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                        myExl = excel.Workbook.Worksheets.Add("sale");
                        myExl.Cells[headerRange].LoadFromArrays(headerRow);
                        myExl.Cells[headerRange].Style.Font.Bold = true;
                        myExl.Cells[headerRange].Style.Font.Size = 12;
                        myExl.Cells.Worksheet.DefaultColWidth = 20;
                        myExl.View.RightToLeft=true;
                        //myExl.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        //string content = "A2:A16";
                        //myExl.Cells[content].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        //  myExl.Workbook.Worksheets.

                        var name = _context.Tprovider.Find(di.provider).Name;
                        //   var date = DateTime.Now;

                        DirectoryInfo dir = new DirectoryInfo(@".\myFiles");
                        try
                        {
                             if(!dir.Exists)
                                dir.Create();
                            FileInfo excelFile = new FileInfo(@".\myFiles\doc" + name + saleId + ".xlsx");

                            var column = 2;
                            foreach (var item in detailsItem)
                            {

                                if (item.provider == di.provider)
                                {
                                    var cellData = new List<object[]>();
                                    var v = _context.TsaleItem.First(f => f.SaleItemId == item.saleItemId);
                                    var q = item.quantity;
                                    if (v.Extra != null)
                                        q = item.quantity - v.Extra;
                                    if (v.ChangeOrderQuantity != null)
                                        q = v.ChangeOrderQuantity;
                                    else
                                    {
                                        if (item.unitsInPackage != null)
                                            q = q / item.unitsInPackage;
                                    }
                                    cellData.Add(new object[] { item.description, item.code, item.nameInFactory, q, item.unitsInPackage });
                                    myExl.Cells[column, 1].LoadFromArrays(cellData);
                                    column++;
                                }
                                if (item.provider > di.provider)
                                {
                                    idP = item.provider;
                                    break;
                                }
                            }
                           


                            tfile = new TfileSale();
                            tfile.Link = excelFile.ToString();
                            tfile.SysRowStatus = 2;
                            tfile.CreatedByUserId = 2;
                            if (!ModelState.IsValid)
                            {
                                //return BadRequest(ModelState);
                            }

                            excel.SaveAs(excelFile);

                            _context.TfileSale.Add(tfile);
                            _context.SaveChanges();


                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.ToString());
                        }

                        //אם הספק מעונין לקבל במייל                          
                        if (_context.Tprovider.Find(idP).SendTypeId == _context.TsendType.First(f => f.Value == "Mail").SendTypeId)
                        {
                            string[] attachment = { @".\myFiles\doc" + name + saleId + ".xlsx" };
                            string[] to = { _context.Tprovider.Find(idP).Mail };
                            Messeage.SendEmail(to, "קובץ לספק להזמנת מכירת העופות", "", false, attachment);
                        }
                    }

                }
                //idP = di.provider;
            }



        }

      // כל מוצר שמשפחות הזמינו באופן כללי של כל המכירות



        public FileSalesController(ChickenSaleContext context)
        {
            _context = context;
        }

        // GET: api/FileSales
        //[HttpGet]
        //public IEnumerable<TfileSale> GetTfileSale()
        //{
        //    return _context.TfileSale;
        //}

        // GET: api/FileSales/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTfileSale([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfileSale = await _context.TfileSale.FindAsync(id);

            if (tfileSale == null)
            {
                return NotFound();
            }

            return Ok(tfileSale);
        }

        // PUT: api/FileSales/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTfileSale([FromRoute] int id, [FromBody] TfileSale tfileSale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tfileSale.FileSaleId)
            {
                return BadRequest();
            }

            _context.Entry(tfileSale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TfileSaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FileSales
        [HttpPost]
        public async Task<IActionResult> PostTfileSale([FromBody] TfileSale tfileSale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.TfileSale.Add(tfileSale);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTfileSale", new { id = tfileSale.FileSaleId }, tfileSale);
        }

        // DELETE: api/FileSales/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTfileSale([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfileSale = await _context.TfileSale.FindAsync(id);
            if (tfileSale == null)
            {
                return NotFound();
            }

            _context.TfileSale.Remove(tfileSale);
            await _context.SaveChangesAsync();

            return Ok(tfileSale);
        }

        private bool TfileSaleExists(int id)
        {
            return _context.TfileSale.Any(e => e.FileSaleId == id);
        }
    }
}