﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Entities.Enum;
using WebChicken.Models;
using WebChicken.Helpers;

namespace WebChicken.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SalesController : ControllerBase
    {
        private readonly ChickenSaleContext _context;
       // ILog log = log4net.LogManager.GetLogger(typeof(SalesController));
        public SalesController(ChickenSaleContext context)
        {
            _context = context;
        }
        // PUT: api/Sales/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTsale([FromRoute] int id, [FromBody] Tsale tsale)
        {
            var item = _context.Tsale.FirstOrDefault(i => i.SaleId == tsale.SaleId);
            if (tsale.Date != null)
                item.Date = tsale.Date;
            if (tsale.LastCheckDate != null)
                item.LastCheckDate = tsale.LastCheckDate;
            if (tsale.FinishDate != null)
                item.FinishDate = tsale.FinishDate;
            _context.SaveChanges();

            if (id != tsale.SaleId)
            {
                return BadRequest();
            }

            tsale.LastModifyDate = DateTime.Now;
            tsale.CreatedByUserId = Convert.ToInt32(User.Identity.Name);
            _context.Entry(tsale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TsaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(_context.Tsale.Where(s => s.SysRowStatus != 0));
        }
        // GET: api/Sales
        [HttpGet]
        public IEnumerable<object> GetTsale()
        {
            List<object> li = new List<object>();
            try
            {
                foreach (var ns in _context.Tsale)
                {
                    if (ns.SysRowStatus != 0)
                    {
                        li.Add(new
                        {
                            SaleId = ns.SaleId,
                            Date = GetHebrewJewishDate1.GetHebrewJewishDateString(DateTime.Parse(ns.Date.ToString())),
                            LastCheckDate = ns.LastCheckDate != null ? GetHebrewJewishDate1.GetHebrewJewishDateString(DateTime.Parse(ns.LastCheckDate.ToString())): string.Empty,
                            NextSaleDate = ns.NextSaleDate != null ? GetHebrewJewishDate1.GetHebrewJewishDateString(DateTime.Parse(ns.NextSaleDate.ToString())): string.Empty,
                            StatusId = _context.TsaleStatus.FirstOrDefault(x => x.StatusId == ns.StatusId).StatusName,
                            FinishDate = ns.FinishDate!=null? GetHebrewJewishDate1.GetHebrewJewishDateString(DateTime.Parse(ns.FinishDate.ToString())):string.Empty ,
                            CreatedByUserId = ns.CreatedByUserId,
                            CreateDate = ns.CreateDate != null ? GetHebrewJewishDate1.GetHebrewJewishDateString(DateTime.Parse(ns.CreateDate.ToString())): string.Empty,
                            LastModifyUserId = ns.LastModifyUserId,
                            SysRowStatus = ns.SysRowStatus,
                            CreatedByUser = ns.CreatedByUser,
                            LastModifyUser = ns.LastModifyUser,
                            Status = ns.Status
                        });
                    }
                }
            }
            catch(Exception ex)
            {
                ex.ToString();
            }
            return li;
        }

        // GET: api/Sales/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTsale([FromRoute] int id)
        {
            //Logger.Debug("Debug message");
            //log.Debug("Debug message");
            //log.Warn("Warn message");
            //log.Error("Error message");
            //log.Fatal("Fatal message");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tsale = await _context.Tsale.FindAsync(id);

            if (tsale == null)
            {
                return NotFound();
            }

            return Ok(tsale);
        }

        //// PUT: api/Sales/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutTsale([FromRoute] int id, [FromBody] Tsale tsale)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != tsale.SaleId)
        //    {
        //        return BadRequest();
        //    }

        //    tsale.LastModifyDate = DateTime.Now;
        //    tsale.CreatedByUserId = Convert.ToInt32(User.Identity.Name);
        //    _context.Entry(tsale).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!TsaleExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return Ok(_context.Tsale.Where(s => s.SysRowStatus != 0));
        //}

        // POST: api/Sales
        [HttpPost]
        public async Task<IActionResult> PostTsale([FromBody] Tsale tsale)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //todo בדיקה שלא מקיימת מכירה פתוחה
            //סטטוס = נפתח
            //add status to the new sale
            tsale.StatusId = (int?)SaleStatus.Opened;
            tsale.CreateDate = DateTime.Now;
            tsale.SysRowStatus = 1;

            tsale.CreatedByUserId = Convert.ToInt32(User.Identity.Name);
        _context.Tsale.Add(tsale);
            try { 
            await _context.SaveChangesAsync();
}
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
            return Ok(_context.Tsale.Where(s => s.SysRowStatus != 0));
        }

        // DELETE: api/Sales/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTsale([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tsale = await _context.Tsale.FindAsync(id);
            if (tsale == null)
            {
                return NotFound();
            }

            tsale.SysRowStatus = 0;
            await _context.SaveChangesAsync();

            return Ok(_context.Tsale.Where(s=>s.SysRowStatus!=0));
        }

        private bool TsaleExists(int id)
        {
            return _context.Tsale.Any(e => e.SaleId == id);
        }
        [HttpGet,Route("CloseSale")]
        public  IActionResult CloseSale()
        {

            CreateInvoices invoice = new CreateInvoices(_context);
            invoice.CreateInvoicesForSale();
            return Ok();
        }

     
    }
}