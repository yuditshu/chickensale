﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TfamilyItemSalesController : ControllerBase
    {

        private readonly ChickenSaleContext _context;

        public TfamilyItemSalesController(ChickenSaleContext context)
        {
            _context = context;
        }

        // GET: api/TfamilyItemSales
        [HttpGet]
        public IEnumerable<TfamilyItemSale> GetTfamilyItemSale()
        {
            return _context.TfamilyItemSale;
        }
        [HttpGet,Route ("GetItemByFamilyAndSale/{id}/{familyId}")]
        public async Task<IActionResult> GetItemByFamilyAndSale([FromRoute] int id,int familyId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfamilyItemSales = _context.TfamilyItemSale.Include(x => x.SaleItem.Item ).Where(x => x.FamilySale.FamilyId == familyId && x.FamilySale.SaleId == id);//.Select( x=>new{ Item=x.SaleItem.Item.Name });

            if (tfamilyItemSales == null)
            {
                return NotFound();
            }

            return Ok(tfamilyItemSales);
        }
        // GET: api/TfamilyItemSales/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTfamilyItemSale([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfamilyItemSale = await _context.TfamilyItemSale.FindAsync(id);

            if (tfamilyItemSale == null)
            {
                return NotFound();
            }

            return Ok(tfamilyItemSale);
        }

        // PUT: api/TfamilyItemSales/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTfamilyItemSale([FromRoute] int id, [FromBody] TfamilyItemSale tfamilyItemSale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tfamilyItemSale.FamilyItemSaleId)
            {
                return BadRequest();
            }

            _context.Entry(tfamilyItemSale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TfamilyItemSaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TfamilyItemSales
        [HttpPost]
        public async Task<IActionResult> PostTfamilyItemSale([FromBody] TfamilyItemSale tfamilyItemSale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.TfamilyItemSale.Add(tfamilyItemSale);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTfamilyItemSale", new { id = tfamilyItemSale.FamilyItemSaleId }, tfamilyItemSale);
        }

        // DELETE: api/TfamilyItemSales/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTfamilyItemSale([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfamilyItemSale = await _context.TfamilyItemSale.FindAsync(id);
            if (tfamilyItemSale == null)
            {
                return NotFound();
            }

            _context.TfamilyItemSale.Remove(tfamilyItemSale);
            await _context.SaveChangesAsync();

            return Ok(tfamilyItemSale);
        }

        private bool TfamilyItemSaleExists(int id)
        {
            return _context.TfamilyItemSale.Any(e => e.FamilyItemSaleId == id);
        }
    }
}