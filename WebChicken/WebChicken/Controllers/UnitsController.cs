﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Helpers;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/units")]
    [ApiController]
    [Authorize(Roles = RolesNames.ADMIN)]
    public class UnitsController : ControllerBase
    {
        private readonly ChickenSaleContext _context;

        public UnitsController(ChickenSaleContext context)
        {
            _context = context;
        }

        // GET: api/Units
        [HttpGet, Route("getUnits")]
        public IActionResult GetTunit()
        {
            List<Tunits> list = _context.Tunits.Where(x => x.SysRowStatus == 1).ToList();
            return Ok(list);
        }

        // GET: api/Units
        [HttpGet]
        public IEnumerable<Tunits> GetTunits()
        {
            return _context.Tunits;
        }

        // GET: api/Units/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTunits([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tunits = await _context.Tunits.FindAsync(id);

            if (tunits == null)
            {
                return NotFound();
            }

            return Ok(tunits);
        }

        // PUT: api/Units/5
        [Route("PutUnit/{id}")]
        public async Task<IActionResult> PutTunits([FromRoute] int id, [FromBody] Tunits tunits)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tunits.UnitId)
            {
                return BadRequest();
            }
            var unit = _context.Tunits.Where(x => x.UnitId == id).FirstOrDefault();
            unit.Plural = tunits.Plural;
            unit.Singular = tunits.Singular;
            unit.IsWeight = tunits.IsWeight;
            unit.LastModifyUserId = int.Parse(User.Identity.Name);
            unit.LastModifyDate = DateTime.Now;
           //_context.Entry(tunits).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException )
            {
                if (!TunitsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(true);
        }

        // POST: api/Units
        [Route("PostUnit")]
        [HttpPost]
        public async Task<IActionResult> PostTunits([FromBody] Tunits tunits)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tunits.Add(tunits);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTunits", new { id = tunits.UnitId }, tunits);
        }

        // DELETE: api/Units/5
        [HttpDelete("deleteUnit/{id}")]
        public async Task<IActionResult> deleteUnit([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tunits = await _context.Tunits.FindAsync(id);
            if (tunits == null)
            {
                return NotFound();
            }

            _context.Tunits.Where(x=> x.UnitId==id).FirstOrDefault().SysRowStatus=0;
            await _context.SaveChangesAsync();

            return Ok(tunits);
        }

        private bool TunitsExists(int id)
        {
            return _context.Tunits.Any(e => e.UnitId == id);
        }
    }
}