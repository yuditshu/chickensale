﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using WebChicken.Helpers;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/Users")]
    [ApiController]
    [Authorize(Roles = RolesNames.ADMIN)]
    public class UsersController : ControllerBase
    {
        private readonly ChickenSaleContext _context;

        public UsersController(ChickenSaleContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("getAllUsers")]
        public IEnumerable<Tusers> getAllUsers()
        {
            return _context.Tusers.Select(u=>new Tusers{UserId=u.UserId, UserName=u.UserName,// Password=u.Password,
                     RoleId=u.RoleId, CreatedByUserId=u.CreatedByUserId, CreateDate=u.CreateDate,
                     LastModifyUserId =u.LastModifyUserId, LastModifyDate=u.LastModifyDate,
                     SysRowStatus =u.SysRowStatus});
            // return _context.Tusers;
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        [Route("GetUserById")]
        public async Task<IActionResult> GetTusers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tusers = await _context.Tusers.FindAsync(id);
            Tusers userFound= (tusers as Tusers);
            Tusers userFoundForSend = new Tusers()
            {
                UserId = userFound.UserId,
                UserName = userFound.UserName,
                Password=userFound.Password,
                RoleId = userFound.RoleId,
                CreatedByUserId = userFound.CreatedByUserId,
                CreateDate = userFound.CreateDate,
                LastModifyUserId = userFound.LastModifyUserId,
                LastModifyDate = userFound.LastModifyDate,
                SysRowStatus = userFound.SysRowStatus
            };
            if (tusers == null)
            {
                return NotFound();
            }
            
            return Ok(userFoundForSend);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTusers([FromRoute] int id, [FromBody] JObject  user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //if (id ==null)
            //{
            //    return BadRequest();
            //}

            var userToEdit = await _context.Tusers.FindAsync(id);
            if (userToEdit == null)
            {
                return NotFound();
            }
            userToEdit.Password = user["newPassword"].ToString();
            //Encryption the password
            //הצפנת הסיסמא
            userToEdit.Password = Encryption.GetSha512Hash(userToEdit.Password);
            //_context.Entry(user).State = EntityState.Modified;
            userToEdit.LastModifyDate = DateTime.Now;
            userToEdit.LastModifyUserId = Convert.ToInt32(User.Identity.Name);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TusersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> PostTusers([FromBody] Tusers user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (CheckEmailIfExists(-1, user.UserName))
            {
                //Encryption the password
                //הצפנת הסיסמא
                user.Password = Encryption.GetSha512Hash(user.Password);
                user.CreatedByUserId =  Convert.ToInt32(User.Identity.Name);
                user.CreateDate = DateTime.Now;
                _context.Tusers.Add(user);
                try
                {
                    await _context.SaveChangesAsync();
                }
                
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            else
            {
                return BadRequest("The email exists in the system");
            }
         
            return Ok(user.UserId);
        }

        /// <summary>
        /// The function check if the email exists
        /// </summary>
        /// <param name="ifPut">if put: the UserId of user,  if post: -1</param>
        /// <param name="stringEmail">the string of email</param>
        /// <returns>true or false, if email exists:false if not:true</returns>
        private  bool CheckEmailIfExists(int ifPut, string stringEmail)
        {
            var r = _context.Tusers.Where(e => e.UserId != ifPut && e.UserName == stringEmail);
            if (_context.Tusers.Where(e => e.UserId != ifPut && e.UserName == stringEmail).Count()==0)
                return true;
            return false;
        }
        // DE
        //LETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTusers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userToDelete = await _context.Tusers.FindAsync(id);
            if (userToDelete == null)
            {
                return NotFound();
            }

            userToDelete.SysRowStatus=0;
            await _context.SaveChangesAsync();

            return Ok(userToDelete.UserId);
        }

        private bool TusersExists(int id)
        {
            return _context.Tusers.Any(e => e.UserId == id);
        }
    }
}