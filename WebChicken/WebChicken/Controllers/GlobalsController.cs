﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Helpers;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/globals")]
    [ApiController]
    [Authorize(Roles = RolesNames.ADMIN)]
    public class GlobalsController : ControllerBase
    {

        private readonly ILog _logger;
        private readonly ChickenSaleContext _context;

        public GlobalsController(ChickenSaleContext context, ILog logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Globals
        [HttpGet,Route("getGlobal")]
        public IActionResult GetTglobal()
        {
            List<Tglobal> list = _context.Tglobal.Where(x => x.SysRowStatus == 1).ToList();
            return Ok(list);
        }
        [HttpDelete, Route("deleteGlobal/{id}")]
        public IActionResult DeleteTglobal(int id)
        {
            _context.Tglobal.FirstOrDefault(x => x.GlobalId == id).SysRowStatus = 0;
            try
            {
                 _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                    throw;
            }
            List<Tglobal> list = _context.Tglobal.Where(x => x.SysRowStatus == 1).ToList();
            return Ok(list);
        }

        // GET: api/Globals/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTglobal([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tglobal = await _context.Tglobal.FindAsync(id);

            if (tglobal == null)
            {
                return NotFound();
            }

            return Ok(tglobal);
        }

        // PUT: api/Globals/5
        [HttpPut("PutGlobal/{id}")]
        public async Task<IActionResult> PutTglobal([FromRoute] int id, [FromBody] Tglobal tglobal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tglobal.GlobalId)
            {
                return BadRequest();
            }


            //_context.Entry(tglobal).State = EntityState.Modified;
            var g = _context.Tglobal.Find(id);
            g.LastModifyDate = DateTime.Now;
            g.CreatedByUserId = Convert.ToInt32(User.Identity.Name);
            g.OldValue = g.Value;
            g.Value = tglobal.Value;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TglobalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(_context.Tglobal.Where(s => s.SysRowStatus != 0));

        }

        // POST: api/Globals
        [HttpPost]
        [Route("PostTglobal")]
        public async Task<IActionResult> PostTglobal([FromBody] Tglobal tglobal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            tglobal.CreateDate = DateTime.Now;
            tglobal.CreatedByUserId = Convert.ToInt32(User.Identity.Name);
            _context.Tglobal.Add(tglobal);
            await _context.SaveChangesAsync();
            return Ok(_context.Tglobal.Where(s => s.SysRowStatus != 0));
           // return CreatedAtAction("GetTglobal", new { id = tglobal.GlobalId }, tglobal);
        }

       
        private bool TglobalExists(int id)
        {
            return _context.Tglobal.Any(e => e.GlobalId == id);
        }
    }
}