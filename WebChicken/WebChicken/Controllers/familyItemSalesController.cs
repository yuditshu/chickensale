﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebChicken.Entities.Enum;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/FamilyItemSales")]
    [ApiController]
    [Authorize]
    public class FamilyItemSalesController : ControllerBase
    {
        private readonly ChickenSaleContext _context;

        public FamilyItemSalesController(ChickenSaleContext context)
        {
            _context = context;
        }
        //api/FamilyItemSales/GetItemByFamilyAndSale/{id}/{familyId}
        [HttpGet, Route("GetItemByFamilyAndSale/{id}/{familyId}")]
        //id של מכירה
        [Authorize]
        public IActionResult GetItemByFamilyAndSale([FromRoute] int id, int familyId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfamilyItemSales = _context.TfamilyItemSale.Include(x => x.SaleItem.Item)
                .Where(x => x.FamilySale.FamilyId == familyId && x.FamilySale.SaleId == id && x.SysRowStatus == 1);


            //.Select( x=>new{ Item=x.SaleItem.Item.Name });

            if (tfamilyItemSales == null)
            {
                return NotFound();
            }

            return Ok(tfamilyItemSales);
        }
        [Authorize]
        [HttpGet, Route("getSaleItemList/{saleId}")]
        public IActionResult getSaleItemList(int saleId)
        {
            var res = _context.TsaleItem.Include(x => x.Item).Where(x => x.SaleId == saleId && x.SysRowStatus == 1);
            return Ok(res);
        }

        // GET: api/FamilyItemSales
        [HttpGet]
        public IEnumerable<TfamilyItemSale> GetTfamilyItemSale()
        {
            return _context.TfamilyItemSale;
        }

        // GET: api/FamilyItemSales/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTfamilyItemSale([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfamilyItemSale = await _context.TfamilyItemSale.FindAsync(id);

            if (tfamilyItemSale == null)
            {
                return NotFound();
            }

            return Ok(tfamilyItemSale);
        }
        [Authorize]
        // PUT: api/FamilyItemSales/5
        [HttpPut, Route("PutTfamilyItemSale/{id}/{familyId}/{saleId}")]
        public async Task<IActionResult> PutTfamilyItemSale([FromRoute] int id, int familyId, int saleId, [FromBody] TfamilyItemSale tfamilyItemSale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tfamilyItemSale.FamilyItemSaleId)
            {
                return BadRequest();
            }
            tfamilyItemSale.LastModifyDate = DateTime.Now;
            tfamilyItemSale.LastModifyUserId = Convert.ToInt32(User.Identity.Name);

            _context.Entry(tfamilyItemSale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TfamilyItemSaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            var tfamilyItemSales = _context.TfamilyItemSale.Include(x => x.SaleItem.Item)
                   .Where(x => x.FamilySale.FamilyId == familyId && x.FamilySale.SaleId == saleId && x.SysRowStatus == 1);


            return Ok(tfamilyItemSales);
        }

        [Authorize]
        // POST: api/FamilyItemSales
        [HttpPost, Route("AddTfamilyItemSale/{saleId}/{familyId}")]
        public async Task<IActionResult> PostTfamilyItemSale(int saleId, int familyId, [FromBody] TfamilyItemSale tfamilyItemSale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //לבדוק אם כבר קיים לו המוצר
            //שרוצה להוסיף ואז רק לעדכן כמות
            int familySaleId = _context.TfamilySale
                .FirstOrDefault(x => x.FamilyId == familyId &&
                x.SaleId == saleId && x.SysRowStatus == 1).FamilySaleId;

            var itmList = _context.TfamilyItemSale.
                Where(x => x.FamilySaleId == familySaleId && x.SysRowStatus == 1).ToList();

            var itm = itmList.FirstOrDefault(x => x.SaleItemId == tfamilyItemSale.SaleItemId);

            if (itm != null)//יש פריט זהה
            {
                itm.Quantity += tfamilyItemSale.Quantity;
            }
            else
            {
                //הוספת פריט חדש   
                tfamilyItemSale.CreateDate = DateTime.Now;

                tfamilyItemSale.CreatedByUserId = Convert.ToInt32(User.Identity.Name);
                tfamilyItemSale.OrderWay = (int?)OrderWay.Secretary;
                tfamilyItemSale.FamilySaleId = familySaleId;
                tfamilyItemSale.SysRowStatus = 1;

                _context.TfamilyItemSale.Add(tfamilyItemSale);
                await _context.SaveChangesAsync();
            }
            var tfamilyItemSales = _context.TfamilyItemSale.Include(x => x.SaleItem.Item)
                .Where(x => x.FamilySale.FamilyId == familyId && x.FamilySale.SaleId == saleId && x.SysRowStatus == 1);


            return Ok(tfamilyItemSales);
        }

        [Authorize]
        // DELETE: api/FamilyItemSales/5
        [HttpDelete, Route("removeTfamilyItemSale/{fis_id}/{saleId}/{familyId}")]
        public async Task<IActionResult> DeleteTfamilyItemSale(int fis_id, int saleId, int familyId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfamilyItemSale = await _context.TfamilyItemSale.FindAsync(fis_id);
            if (tfamilyItemSale == null)
            {
                return NotFound();
            }
            tfamilyItemSale.SysRowStatus = 0;
            tfamilyItemSale.LastModifyUserId = Convert.ToInt32(User.Identity.Name);
            tfamilyItemSale.LastModifyDate = DateTime.Now;

            //_context.TfamilyItemSale.Remove(tfamilyItemSale);
            await _context.SaveChangesAsync();


            var tfamilyItemSales = _context.TfamilyItemSale.Include(x => x.SaleItem.Item)
                           .Where(x => x.FamilySale.FamilyId == familyId && x.FamilySale.SaleId == saleId && x.SysRowStatus == 1);


            return Ok(tfamilyItemSales);
        }

        private bool TfamilyItemSaleExists(int id)
        {
            return _context.TfamilyItemSale.Any(e => e.FamilyItemSaleId == id);
        }
    }
}
