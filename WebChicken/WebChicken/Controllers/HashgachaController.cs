﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Helpers;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/hashgacha")]
    [ApiController]
    [Authorize(Roles = RolesNames.ADMIN)]
    public class HashgachaController : ControllerBase
    {
       private readonly ChickenSaleContext _context;    


        public HashgachaController(ChickenSaleContext context)
        {
            _context = context;
        }

        [HttpGet, Route("getHashgacha")]
        public IActionResult GetThashgacha()
        {
            List<Thashgacha> list = _context.Thashgacha.Where(x => x.SysRowStatus == 1).ToList();
            return Ok(list);
        }

         [HttpGet("{id}")]
        public async Task<IActionResult> GetThashgacha([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var thashgacha = await _context.Thashgacha.FindAsync(id);

            if (thashgacha == null)
            {
                return NotFound();
            }

            return Ok(thashgacha);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutThashgacha([FromRoute] int id, [FromBody] Thashgacha thashgacha)
        {
            // העדכון לא נראה לי עובד!!!!????
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != thashgacha.HashgachId)
            {
                return BadRequest();
            }
            thashgacha.LastModifyDate = DateTime.Now;
            thashgacha.LastModifyUserId = Convert.ToInt32(User.Identity.Name);
            _context.Entry(thashgacha).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ThashgachaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> PostThashgacha([FromBody] Thashgacha thashgacha)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            thashgacha.CreatedByUserId = Convert.ToInt32(User.Identity.Name);
            _context.Thashgacha.Add(thashgacha);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetThashgacha", new { id = thashgacha.HashgachId }, thashgacha);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteThashgacha([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var thashgacha = await _context.Thashgacha.FindAsync(id);
            if (thashgacha == null)
            {
                return NotFound();
            }
            _context.Thashgacha.FirstOrDefault(x => x.HashgachId == id).SysRowStatus = 0;
            //_context.Thashgacha.Remove(thashgacha);
            await _context.SaveChangesAsync();

            return Ok(thashgacha);
        }

        private bool ThashgachaExists(int id)
        {
            return _context.Thashgacha.Any(e => e.HashgachId == id);
        }
    }
}