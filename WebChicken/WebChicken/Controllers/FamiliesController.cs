﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/Families")]
    [ApiController]
    [Authorize]
    public class FamiliesController : ControllerBase
    {
        ChickenSaleContext db = new ChickenSaleContext();
        private readonly ChickenSaleContext _context;

        public FamiliesController(ChickenSaleContext context)
        {

            _context = context;
        }

        // GET: api/Families

        [Authorize]
        [Route("getAllFamilies")]
        [HttpGet]
        public IActionResult GetTfamily()
        {
            List<Tfamily> list = _context.Tfamily.Where(x => x.SysRowStatus == 1).ToList();
            return Ok(list);
            //db.Tfamily.ToList()
            //return _context.Tfamily;
        }

        // GET: api/Families/5
        [Authorize]
        [HttpGet]
        [Route("getFamilyById/{id}")]
        public async Task<IActionResult> GetTfamily(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfamily = await _context.Tfamily.FindAsync(id);

            if (tfamily == null)
            {
                return NotFound();
            }

            return Ok(tfamily);
        }

        // PUT: api/Families/5
        [HttpPut, Route("editFamily/{id}")]
        [Authorize]
        public async Task<IActionResult> PutTfamily([FromRoute] int id, [FromBody] Tfamily tfamily)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tfamily.FamilyId)
            {
                return BadRequest();
            }

            //בדיקה שאין את אותו מייל כבר במערכת
            Tfamily f = _context.Tfamily.FirstOrDefault(x => x.Mail == tfamily.Mail);
            if (f != null)//יש כבר כתובת כזאת במערכת
                return Ok(false);

            tfamily.LastModifyDate = DateTime.Now;
            tfamily.LastModifyUserId = Convert.ToInt32(User.Identity.Name);
            _context.Entry(tfamily).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TfamilyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(tfamily);
        }

        // POST: api/Families/addFamily
        [Route("addFamily")]
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostTfamily([FromBody] Tfamily tfamily)
        {


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //בדיקה שאין את אותו מייל כבר במערכת
            Tfamily f = _context.Tfamily.FirstOrDefault(x => x.Mail == tfamily.Mail);
            if (f != null)//יש כבר כתובת כזאת במערכת
                return Ok(false);

            tfamily.CreatedByUserId = Convert.ToInt32(User.Identity.Name);
            tfamily.CreateDate = DateTime.Now;
            tfamily.SysRowStatus = 1;
            _context.Tfamily.Add(tfamily);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TfamilyExists(tfamily.FamilyId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return Ok(tfamily);
        }

        // DELETE: api/Families/5
        [HttpDelete, Route("removeFamily/{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteTfamily([FromRoute] int id)
        {
            //לשנות רק את sysRowStatus 
            //בלי למחוק את הרשומה באמת
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tfamily = await _context.Tfamily.FindAsync(id);
            if (tfamily == null)
            {
                return NotFound();
            }
            tfamily.SysRowStatus = 0;

            //_context.Tfamily.Remove(tfamily);
            await _context.SaveChangesAsync();

            return Ok(_context.Tfamily.Where(x => x.SysRowStatus == 1).ToList());
        }

        [Authorize]
        private bool TfamilyExists(int id)
        {
            return _context.Tfamily.Any(e => e.FamilyId == id);
        }
    }
}