﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProvidersController : ControllerBase
    {
        private readonly ChickenSaleContext _context;

        public ProvidersController(ChickenSaleContext context)
        {
            _context = context;
        }

        // GET: api/Providers
        [HttpGet]
        public IEnumerable<Tprovider> GetTprovider()
        {
            return _context.Tprovider;
        }

        // GET: api/Providers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTprovider([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tprovider = await _context.Tprovider.FindAsync(id);

            if (tprovider == null)
            {
                return NotFound();
            }

            return Ok(tprovider);
        }

        // PUT: api/Providers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTprovider([FromRoute] int id, [FromBody] Tprovider tprovider)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tprovider.ProviderId)
            {
                return BadRequest();
            }

            _context.Entry(tprovider).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TproviderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Providers
        //[Route("AddProvider")]
        [HttpPost]
        public async Task<IActionResult> PostTprovider([FromBody] Tprovider tprovider)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tprovider.Add(tprovider);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTprovider", new { id = tprovider.ProviderId }, tprovider);
        }

        // DELETE: api/Providers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTprovider([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tprovider = await _context.Tprovider.FindAsync(id);
            if (tprovider == null)
            {
                return NotFound();
            }

            _context.Tprovider.Remove(tprovider);
            await _context.SaveChangesAsync();

            return Ok(tprovider);
        }

        private bool TproviderExists(int id)
        {
            return _context.Tprovider.Any(e => e.ProviderId == id);
        }
    }
}