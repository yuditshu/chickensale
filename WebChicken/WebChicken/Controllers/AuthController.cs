﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Login.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using WebChicken.Helpers;
using WebChicken.Models;

namespace WebChicken.Controllers
{
    /// <summary>
    /// controller to log-in and authentication
    /// </summary>
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ChickenSaleContext _context;

        public AuthController(ChickenSaleContext context)
        {
            _context = context;
        }

        /// <summary>
        /// verify name and password
        /// </summary>
        /// <param name="user"></param>
        /// <returns>userId</returns>
        private  Tusers VerifyPassword(LogInUser user)
        {
            //call to function in the Encryption class to encrypt the password
            var encryptedPass = Encryption.GetSha512Hash(user.Password);
            //check if the encrypted password is exists in the DB
         
            return _context.Tusers.Where(p => p.UserName == user.UserName && p.Password == encryptedPass).FirstOrDefault();

        }

        /// <summary>
        /// login user with name and password and create token to user
        /// </summary>
        /// <param name="loginUser"></param>
        /// <returns>token</returns>
        [AllowAnonymous]
        [HttpPost("login")]

        public IActionResult Login([FromBody] LogInUser loginUser)
        {
            var user = VerifyPassword(loginUser);
            if(user!=null)
            {
                var token = GenerateTokenForUser(user);
                return Ok(new { token, role=user.RoleId });
            }
            return Unauthorized();
        }

        /// <summary>
        /// generate token to user
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userId"></param>
        /// <returns>signedAndEncodedToken</returns>
        public string GenerateTokenForUser(Tusers user)
        {
            var signingKey = "GQDstc21ewfffffffffffFiwDffVvVBrk";
            var now = DateTime.UtcNow;
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(signingKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var claimsIdentity = new ClaimsIdentity(new List<Claim>()
            {
                new Claim(ClaimTypes.Name,  user.UserId.ToString()),
                new Claim(ClaimTypes.Role, user.RoleId.ToString()),//RoleId //todo
        }, "Custom");
            
            var securityTokenDescriptor = new SecurityTokenDescriptor()
            {
                Issuer = "self",
                Subject = claimsIdentity,
                SigningCredentials = creds,
                Expires = now.AddMinutes(20),
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var plainToken = tokenHandler.CreateToken(securityTokenDescriptor);
            var signedAndEncodedToken = tokenHandler.WriteToken(plainToken);

            return signedAndEncodedToken;

        }

        /// <summary>
        /// init password and send a message to the user with new password
        /// </summary>
        /// <param name="userName"></param>
        [AllowAnonymous]
        [HttpGet("initpass/{userName}")]
        public void InitPassword(string userName)
        {
            var encryptPass = "";
            string passRandon = "";
            //random password by the class RandomPassword and check that it isn't exists in the DB
            do
            {
                passRandon = RandomPassword.CreateRandomPassword();
                encryptPass = Encryption.GetSha512Hash(passRandon.ToString());

           } while(_context.Tusers.FirstOrDefault(u => u.Password == encryptPass)!=null);

            //send a message with the new password to the user E-mail
            string[] email = new string[1];
            email[0] = userName;
            //save the new password in the DB
            if (Messeage.SendEmail(email, "איפוס סיסמא", "סיסמתך החדשה הינה " + passRandon + " הנך יכוך לשנות את הססמא באתר."))
                _context.Tusers.First(u => u.UserName == userName).Password = encryptPass;
            _context.SaveChanges();

        }

      
     

    }
}