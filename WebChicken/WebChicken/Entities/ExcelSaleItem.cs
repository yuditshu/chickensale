﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Entities
{
    public class ExcelSaleItem
    {


        //קוד תחנה(אין התייחסות בDB),קוד קטגוריה, סדר בטלפון לקטגוריה, שם קטגוריה,קוד מוצר, שם מוצר,הערות,הערות להדפסה, סדר בטלפון,יחידה ,מחיר,לפי משקל


        public int StationId { get; set; }
        public int CategoryId { get; set; }
        public int OrderCategories { get; set; }
        public string CategoryName { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string Remark { get; set; }
        public string PrintRemark { get; set; }
        public int OrderItems { get; set; }
        public string Unit { get; set; }
        public decimal Price { get; set; }
        public bool IsWeight { get; set; }
        
        //        קוד בתפריט הטלפוני ,שם קטגוריה, שם מוצר,הערות,יחידה,מוצר,לפי משקל, כמות
        //{"CategoryId":"1","PhoneQueuetoCategory":"1","CategoryName":"aa","ItemId":"2","ItemName":"bb","Remark":"c",
        //"PrintRemark":"1","PhoneQueue":"2","Unit":"9","Price":"9","IsWeight":"true"}

    }
    }



