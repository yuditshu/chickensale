﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Entities
{
    public class FamilyExcelItem
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string KategoryName { get; set; }
        public decimal Price { get; set; }
        public int NumberOfUnits { get; set; }
        public DateTime LastChange { get; set; }
        public int FamilyId { get; set; }
        public string FullName { get; set; }
        public string PhonesNumbers { get; set; }


        public static Dictionary<string, string> getColumnsNames()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();

            dic.Add("ItemId", "קוד מוצר");
            dic.Add("ItemName", "שם מוצר");
            dic.Add("KategoryName", "קטגוריה");
            dic.Add("Price", "מחיר מוצר");
            dic.Add("NumberOfUnits", "כמות");
            dic.Add("LastChange", "שינוי אחרון");
            dic.Add("FamilyId", "קוד לקוח");
            dic.Add("FullName", "שם מלא");
            dic.Add("PhonesNumbers", "טלפונים");
        
            return dic;
        }

    }
}
