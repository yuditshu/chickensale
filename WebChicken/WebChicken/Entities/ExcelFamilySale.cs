﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Entities
{
    public class ExcelFamilySale
    {
        //קוד בתפריט הטלפוני	שם קטגוריה	שם מוצר	הערות	יחידה	מוצר	לפי משקל	כמות
        public int Cod { get; set; }
        public int CodMenuPhone { get; set; }
        public string  NameKategory { get; set; }
        public string NameItem { get; set; }
        public string Note { get; set; }
        public string Unit { get; set; }
        public Boolean IsByWeight { get; set; }
        public int Quantity { get; set; }


        public static Dictionary<string, string> GetColumnsName()
        {
            return new Dictionary<string, string>()
            {
                {"Cod","קוד מוצר" },
                {"CodMenuPhone","קוד בתפריט הטלפוני" },
                {"NameKategory","שם קטגוריה" },
                {"NameItem","שם מוצר"  },
                {"Note","הערות"  },
                {"Unit","יחידה"  },
                {"IsByWeight","מוצר לפי משקל" },
                {"Quantity","כמות" }
            };
        }
    }
}
