﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace WebChicken.Helpers
{
    public class Messeage
    {
        //פונקציה זו מקבלת רשימת נמענים לשליחה 
        //מקבלת נושא אימייל ותוכן
        //=לא חובה וקבצים מצורפים לשליחה
        //מחזיר true אם הצליח 
        //מחזיר false אם לא הצליח
        //אם התצוגה של ההודעה תיהיה HTML showHtml מקבל 

        public static bool SendEmail(string[] to, string subject, string content,bool showHtml=false, string[] attachmentFilename = null)

        {
            SmtpClient smtpClient = new SmtpClient();
            var basicCredential = new NetworkCredential("chickensale1234@gmail.com", "ch1234sale");
            MailMessage message = new MailMessage();
            MailAddress fromAddress = new MailAddress("chickensale1234@gmail.com");

            smtpClient.Host = "smtp.gmail.com";
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = basicCredential;
            smtpClient.EnableSsl = true;


            message.From = fromAddress;
            message.Subject = subject;

            message.Body = content;


            //אם קיימים קבצים לשליחה
            if (attachmentFilename != null)
            {
                //עובר על מערך הקבמים ומוסיף אותם להודעה
                foreach (string file in attachmentFilename)
                {
                    message.Attachments.Add(new Attachment(file));
                }
            }
            // HTMLאם תצוגת ההודעה תיהיה עי  
            if (showHtml==true)
            {
                message.IsBodyHtml = true;
            }


            //עובר על רשימת הנמענים
            foreach (var item in to)
            {
                message.To.Add(item);
            }

            try
            {
                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                //Error, could not send the message
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
    }
}
