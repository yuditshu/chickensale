﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Helpers
{
    public class GetHebrewJewishDate
    {
        //מקבל תאריך לועזי ומחזיר אותו בעברי
        public static string GetHebrewJewishDateString(DateTime anyDate)
        {
            System.Text.StringBuilder hebrewFormatedString = new System.Text.StringBuilder();
            // Create the hebrew culture to use hebrew (Jewish) calendar 
            CultureInfo jewishCulture = CultureInfo.CreateSpecificCulture("he-IL");
            jewishCulture.DateTimeFormat.Calendar = new HebrewCalendar();

            // Day of the week in the format " " 
            hebrewFormatedString.Append(anyDate.ToString("dddd", jewishCulture) + " ");
            // Day of the month in the format "'" 
            hebrewFormatedString.Append(anyDate.ToString("dd", jewishCulture) + " ");
            // Month and year in the format " " 
            hebrewFormatedString.Append("" + anyDate.ToString("y", jewishCulture));

            return hebrewFormatedString.ToString();
        }

        public static string GetHebrewJewishDateWithOutDayInWeekString(DateTime anyDate)
        {
            System.Text.StringBuilder hebrewFormatedString = new System.Text.StringBuilder();
            CultureInfo jewishCulture = CultureInfo.CreateSpecificCulture("he-IL");
            jewishCulture.DateTimeFormat.Calendar = new HebrewCalendar();

            hebrewFormatedString.Append(anyDate.ToString("dd", jewishCulture) + " ");
            hebrewFormatedString.Append("" + anyDate.ToString("y", jewishCulture));

            return hebrewFormatedString.ToString();
        }
    
        public static string getEnglishDate(DateTime date)
        {
            return date.Day + "/" + date.Month + "/" + date.Year;
        }
    }
}
