﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Helpers
{
    public class SaleTimeNotCorrectException : Exception
    {
        public SaleTimeNotCorrectException(string message) : base(message)
        {

        }
    }
}
