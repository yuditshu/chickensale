﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Helpers
{
    public class ConvertBooleanToVX
    {
        public static string Convert(bool? flag)
        {
            if (flag==true)
                return "✔";
            return "✖";
        }
    }
}
