﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Helpers
{
    public class SaleSummaryItem
    {
        public int Extra { get; set; }
        public int ChangeOrderQuantity { get; set; }
        public bool IsCome { get; set; }
    }
}
