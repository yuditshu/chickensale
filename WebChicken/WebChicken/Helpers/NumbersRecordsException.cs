﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Helpers
{
    public class NotCorrectFormatFileException : Exception
    {
        public NotCorrectFormatFileException(string message) : base(message)
        {

        }
    }
}
