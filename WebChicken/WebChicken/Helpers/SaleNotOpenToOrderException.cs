﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Helpers
{
    public class SaleNotOpenToOrderException : Exception
    {
        public SaleNotOpenToOrderException(string message) : base(message)
        {

        }
    }
}
