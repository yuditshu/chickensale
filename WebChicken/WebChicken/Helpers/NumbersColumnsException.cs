﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Helpers
{
    public class NumbersColumnsException : Exception
    {
        public NumbersColumnsException(string message) : base(message)
        {

        }
    }
}
