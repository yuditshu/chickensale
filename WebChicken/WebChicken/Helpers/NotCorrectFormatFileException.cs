﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChicken.Helpers
{
    public class NumbersRecordsException : Exception
    {
        public NumbersRecordsException(string message) : base(message)
        {

        }
    }
}
