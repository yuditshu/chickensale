﻿using Microsoft.AspNetCore.Hosting.Internal;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using WebChicken.Entities;

namespace WebChicken.Helpers
{
    public class ExcelParser<T> where T : class, new()
    {       
        public static List<T> ReadDocument(string path, Dictionary<string, string> namesOfColumnsInClassAndFile) 
        {
            HostingEnvironment _hostingEnvironment = new HostingEnvironment();
            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            application.DefaultVersion = ExcelVersion.Excel2016;
            string basePath = _hostingEnvironment.WebRootPath + path ;
            FileStream sampleFile = new FileStream(basePath, FileMode.Open);
            IWorkbook workbook = application.Workbooks.Open(sampleFile);
            IWorksheet worksheet = workbook.Worksheets[0];
            if (worksheet.Rows.Count() > 100000)
                throw new NumbersRecordsException("מספר הרשומות חורג מהגודל הרגיל");
            if (worksheet.Columns.Count() > namesOfColumnsInClassAndFile.Count)
                throw new NumbersColumnsException("מספר העמודות חורג מהמספר הנדרש");

            int numCol = 1;
            foreach (var he in namesOfColumnsInClassAndFile.Values)
            {
                if (he != worksheet.Range[1, numCol++].Value)
                    throw new NotCorrectFormatFileException("הקובץ אינו בפורמט הנדרש");
            }

            List<T> saleItems = new List<T>();

            var props = typeof(T).GetProperties().ToList();

             T saleItem;

          int countRows = worksheet.Rows.Count();
            ///
            ///מעבר על כל הרשומות בטבלה 
            ///דילוג על הרשומה הראשונה כי היא הכותרת לכן היא לא נבדקת
            ///
            for (int r = 2; r < countRows; r++)
            {
                ///יצירת אובייקט חדש
                saleItem = new T();

                numCol = 1;

                ///מעבר על כל המשתנים (חברי המחלקה) של מחלקת האובייקט
                foreach (PropertyInfo prop in typeof(T).GetProperties())
                {
                    ///בדיקה איזה סוג הערך בתא הטבלה
                   ///והכנסתו לחבר המחלקה
                    if (prop.PropertyType.Name == "Int32")
                  prop.SetValue(saleItem, int.Parse(worksheet.Range[r, numCol++].Value));
                     else 
                        if(prop.PropertyType.Name == "Decimal")
                        prop.SetValue(saleItem, decimal.Parse(worksheet.Range[r, numCol++].Value));
                    else
                        if(prop.PropertyType.Name == "Boolean")
                        prop.SetValue(saleItem, (worksheet.Range[r, numCol++].Value == "כן")? true : false);
                     else

                    prop.SetValue(saleItem,worksheet.Range[r, numCol++].Value);
                }


                saleItems.Add(saleItem);
            }

            return saleItems;
        }

    }
}
