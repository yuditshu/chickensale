﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebChicken.Entities;
using WebChicken.Helpers;
using WebChicken.Models;
using SelectPdf;
using WebChicken.Entities.Enum;

namespace WebChicken.Helpers
{
    public class CreateInvoices
    {
        private readonly ChickenSaleContext _context;

        public CreateInvoices(ChickenSaleContext context)
        {
            _context = context;
        }

        /// <summary>
        /// יצירת חשבוניות לכל הקונים במכירה האחרונה וסגירת המכירה
        /// </summary>
        /// <returns>מחזיר את מספר החשבוניות שנוצרו</returns>
        public  string CreateInvoicesForSale()//static
        {
            #region שליפת קוד המכירה וכל ההזמנות של המשפחות
           // TsaleStatus status = _context.TsaleStatus.First(st => st.SysRowStatus == 1 && st.StatusName == "פתוח להזמנות");
            Tsale sale = _context.Tsale.Last(sa => (sa.StatusId ==(int)SaleStatus.Ended|| sa.StatusId == (int)SaleStatus.OpenForOrders) && sa.SysRowStatus == 1);
            List<TfamilySale> familySales = _context.TfamilySale.Where(fs => fs.SysRowStatus == 1 && fs.SaleId == sale.SaleId).ToList();
            #endregion

            #region מעבר על כל המשפחות שהזמינו במכירה האחרונה הפתוחה ויצירת החשבונית שלהן
            foreach (TfamilySale tfamilySale in familySales)
            {
                #region הגדרת משתנים
                List<TfamilyItemSale> tfamilyItemSale;
                Tfamily tfamily = _context.Tfamily.FirstOrDefault(f => f.FamilyId == tfamilySale.FamilyId);

                tfamilyItemSale = _context.TfamilyItemSale.Where(p => p.FamilySaleId == tfamilySale.FamilySaleId).ToList();
                List<TsaleItem> tsaleItem = _context.TsaleItem.Where(i => i.SaleId == tfamilySale.SaleId).ToList();
                string remarkTitle, remark1, remark2, remark3, remark4, remark5, remark6, remark7, details, family, phone, findTheProducts, numProd, theProd, supervising, color, arrow, whightInKg, priceForUnit, priceForAmount, tableValue, sumForPay, creditDebit;
                List<TfamilyItemSale> notCome = new List<TfamilyItemSale>();
                string tableNotCome = "";


                #endregion

                #region הצבת נתונים להחלפה
                remarkTitle = "הערות חשובות:";
                remark1 = "חובה להדביק את המדבקה של הקרטונים (המכילה שם מוצר ומשקל) על דף ההזמנה";
                remark2 = "מוצרים שלא רשום עליהם משקל- יש לגשת לעמדת השקילה (מסומנים ברשימה בכוכביות **)";
                remark3 = "חובה לבדוק את החשבון בבית!!!";
                remark4 = "נודה מאד לתשלום במזומן!";
                remark5 = "לרושמים צ'ק: יש לרשום את הצ'ק בשקלים שלמים בלבד!!";
                remark6 = "תאריך אחרון לרישום הצ'קים: ";
                remark6 += GetHebrewJewishDate.GetHebrewJewishDateWithOutDayInWeekString(sale.LastCheckDate.Value) + " " + GetHebrewJewishDate.getEnglishDate(sale.LastCheckDate.Value);
                remark7 = "המכירה הבאה תתקיים בעז" + '"' + "ה ב" +
                GetHebrewJewishDate.GetHebrewJewishDateWithOutDayInWeekString(sale.NextSaleDate.Value)
                + "   " + GetHebrewJewishDate.getEnglishDate(sale.NextSaleDate.Value)
                    + ", הודעה תתקבל מקו ההודעות 073-3372252";
                details = "מכירת עופות ובשר<br>בחסות קופת השכונה בית וגן";
                family = "<b>משפחה: </b>" + tfamily.FamilyId.ToString() + " , " + tfamily.Name;
                if (tfamily.Phone2 != null || tfamily.Phone2 != "")
                    phone = "<b>טלפון: </b>" + tfamily.Phone + " , " + tfamily.Phone2;
                else
                    phone = "<b>טלפון: </b>" + tfamily.Phone;
                findTheProducts = "אתרו את המוצרים לפי <span style='background-color:black; color:white;'>הקוד</span> המופיע על גבי השלטים";
                numProd = "כמות מוזמנת";
                theProd = "המוצר";
                supervising = "השגחה";
                color = "צבע";
                arrow = "⬇";
                whightInKg = "משקל בק\"ג";
                priceForUnit = "מחיר ליחידה";
                priceForAmount = "מחיר לכמות";
                tableValue = "";
                sumForPay = "<b style='margin-left:100px;'>סך הכל לתשלום: </b>ש\"ח";
                bool even = false;
                creditDebit = "זיכויים / חיובים";
                #endregion

                ///יצירת הטבלה של המוצרים שהוזמנו
                foreach (var item in tfamilyItemSale)
                {
                    TsaleItem saleitem = tsaleItem.Find(i => i.SaleItemId == item.SaleItemId.Value);
                    Titem titem = _context.Titem.Find(saleitem.ItemId);
                   // Thashgacha hashgacha = _context.Thashgacha.First(h => h.HashgachId == titem.HashgachId);
                    var weight = (_context.Tunits.Find(titem.UnitId).IsWeight == true) ? "**" : "------";
                    var unit = (item.Quantity == 1) ? titem.Unit.Singular : titem.Unit.Plural;
                    if (saleitem.IsCome == false)
                    {
                        notCome.Add(item);
                        continue;
                    }
                    if (even == false)
                        even = true;
                    else
                        even = false;
                    if (even == true)
                        tableValue += "<tr><td>";
                    else
                        tableValue += "<tr style='background-color: #eeecec;'><td>";
                    tableValue +=
                        item.Quantity + " " + unit +
                           "</td><td>" +
                           titem.Name +
                           "</td><td>" +
                           titem.Hashgach?.NameHashgach +
                           "</td><td>" +
                           titem.Hashgach?.Color +
                           "</td><td>" +
                           saleitem.ItemId +
                           "</td><td>" +
                           weight +
                           "</td><td>" +
                           titem.Price +
                           "</td><td>";
                    if (weight == "**")
                        tableValue += "";
                    else
                        tableValue += titem.Price * item.Quantity;
                    tableValue += "</td></tr>";
                }
                ///יצירת הטבלה של המוצרים שלא הגיעו
                if (notCome.Count() > 0)
                {
                    even = false;
                    foreach (var item in notCome)
                    {
                        TsaleItem saleitem = tsaleItem.Find(i => i.SaleItemId == item.SaleItemId.Value);
                        Titem titem = _context.Titem.Find(saleitem.ItemId);
                        //Thashgacha hashgacha = _context.Thashgacha.First(h => h.HashgachId == titem.HashgachId);
                        var weight = (_context.Tunits.Find(titem.UnitId).IsWeight == true) ? "**" : "----------";
                        var unit = (item.Quantity == 1) ? titem.Unit.Singular : titem.Unit.Plural;
                        if (even == false)
                            even = true;
                        else
                            even = false;
                        if (even == true)
                            tableNotCome += "<tr><td>";
                        else
                            tableNotCome += "<tr style='background-color: #eeecec;'><td>";
                        tableNotCome +=
                            item.Quantity + " " + unit +
                               "</td><td>" +
                               titem.Name +
                               "</td><td>" +
                              titem. Hashgach?.NameHashgach +
                               "</td><td>";
                        if (saleitem.IfPrintSign == true)
                            tableNotCome += saleitem.FormRemark;
                        else
                            tableNotCome += "";
                        tableNotCome += "</td></tr>";
                    }
                }

                ///קריאת כל התוכן של החשבונית
                string s = System.IO.File.ReadAllText("helpers/invoice.html");

                #region html החלפות טקסט בתוך קובץ ה 

                s = s.Replace("@bsd", "בס\"ד");
                s = s.Replace("@details", details);
                s = s.Replace("@date", GetHebrewJewishDate1.GetHebrewJewishDateString(DateTime.Now));
                s = s.Replace("@remark1", remark1);
                s = s.Replace("@remark2", remark2);
                s = s.Replace("@remark3", remark3);
                s = s.Replace("@remark4", remark4);
                s = s.Replace("@remark5", remark5);
                s = s.Replace("@remark6", remark6);
                s = s.Replace("@remark7", remark7);
                s = s.Replace("@remarkTitle", remarkTitle);
                s = s.Replace("@family", family);
                s = s.Replace("@phone", phone);
                s = s.Replace("@findTheProducts", findTheProducts);
                s = s.Replace("@numProd", numProd);
                s = s.Replace("@theProd", theProd);
                s = s.Replace("@supervising", supervising);
                s = s.Replace("@whightInKg", whightInKg);
                s = s.Replace("@color", color);
                s = s.Replace("@arrow", arrow);
                s = s.Replace("@priceForUnit", priceForUnit);
                s = s.Replace("@priceForAmount", priceForAmount);
                s = s.Replace("@tableValue", tableValue);
                s = s.Replace("@credit/debit:", creditDebit);
                s = s.Replace("@credit/debitCause", tfamilySale.CreditDebitCause);
                if (tfamilySale.CreditDebit > 0)
                    s = s.Replace("@credit/debitValue", tfamilySale.CreditDebit.ToString());
                else
                    s = s.Replace("@credit/debitValue", Math.Abs(tfamilySale.CreditDebit) + "-");
                s = s.Replace("@sumForPay", sumForPay);
                s = s.Replace("@sumForExpenseCovering", "<b>לכיסוי ההוצאות: </b> 2 ש\"ח");
                s = s.Replace("@remarkNotCome", "הערה");
                s = s.Replace("@prodsNotCome", "😢 מוצרים שלא הגיעו 😢");
                s = s.Replace("@notCome", tableNotCome);

                #endregion

                #region יצירת הקובץ ושמירתו
                ///יצירת אוביקט של המרה 
                HtmlToPdf converter = new HtmlToPdf();
                ///מיקום הקובץ שישמר
                string nameInvoice = tfamilySale.FamilyId.ToString();
                string file = @"MyFiles/MyInvoice/" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + "/" + nameInvoice + ".pdf";
                ///(הגדרות של הקובץ (גודל ועוד
                converter.Options.PdfPageSize = PdfPageSize.A4;
                converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
                converter.Options.MarginTop = 10;
                converter.Options.MarginBottom = 10;
                converter.Options.MarginLeft = 10;
                converter.Options.MarginRight = 10;
                converter.Options.DisplayFooter = true;
                converter.Footer.TotalPagesOffset = 2;
                converter.Footer.FirstPageNumber = 2;
                ///יצירת אויבקט מסוג קובץ
                PdfDocument doc = converter.ConvertHtmlString(s);
                ///שמירת הקובץ במיקום הנדרש
                doc.Save(file);
                #endregion
            }
            #endregion

            #region שינוי סטטוס המכירה לסגור
            // _context.Tsale.First(ss => ss.SaleId == sale.SaleId).StatusId = _context.TsaleStatus.First(sts => sts.SysRowStatus == 1 && sts.StatusName == "סגור להזמנות").StatusId;
            sale.StatusId = (int)SaleStatus.Closed;
            _context.SaveChanges();
            #endregion

           return "מספר הקבצים שנוצרו הוא: " + familySales.Count();
        }
    }
}
