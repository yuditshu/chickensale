﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class TfamilyItemSaleHistory
    {
        public int FamilyItemSaleId { get; set; }
        public int? FamilySaleId { get; set; }
        public int? SaleItemId { get; set; }
        public int? Quantity { get; set; }
        public int? OrderWay { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public TfamilySale FamilySale { get; set; }
        public TorderWay OrderWayNavigation { get; set; }
        public TsaleItem SaleItem { get; set; }
    }
}
