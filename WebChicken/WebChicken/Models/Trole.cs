﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class Trole
    {
        public Trole()
        {
            Tusers = new HashSet<Tusers>();
        }

        public int RoleId { get; set; }
        public string RoleType { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public int? SysRowStatus { get; set; }
        public DateTime LastModifyDate { get; set; }

        public Tusers LastModifyUser { get; set; }
        public ICollection<Tusers> Tusers { get; set; }
    }
}
