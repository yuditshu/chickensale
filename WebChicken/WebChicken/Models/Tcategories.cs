﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class Tcategories
    {
        public Tcategories()
        {
            Titem = new HashSet<Titem>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int OrderInSys { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Tusers LastModifyUser { get; set; }
        public ICollection<Titem> Titem { get; set; }
    }
}
