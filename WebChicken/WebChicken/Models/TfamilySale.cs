﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class TfamilySale
    {
        public TfamilySale()
        {
            TfamilyItemSale = new HashSet<TfamilyItemSale>();
            TfamilyItemSaleHistory = new HashSet<TfamilyItemSaleHistory>();
        }

        public int FamilySaleId { get; set; }
        public int FamilyId { get; set; }
        public int SaleId { get; set; }
        public decimal CreditDebit { get; set; }
        public string CreditDebitCause { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Tfamily Family { get; set; }
        public TfamilySale FamilySale { get; set; }
        public Tusers LastModifyUser { get; set; }
        public Tsale Sale { get; set; }
        public TfamilySale InverseFamilySale { get; set; }
        public ICollection<TfamilyItemSale> TfamilyItemSale { get; set; }
        public ICollection<TfamilyItemSaleHistory> TfamilyItemSaleHistory { get; set; }
    }
}
