﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class TsaleStatus
    {
        public TsaleStatus()
        {
            Tsale = new HashSet<Tsale>();
        }

        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Tusers LastModifyUser { get; set; }
        public ICollection<Tsale> Tsale { get; set; }
    }
}
