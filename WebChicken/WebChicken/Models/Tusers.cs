﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebChicken.Models
{
    public partial class Tusers
    {
        public Tusers()
        {
            InverseCreatedByUser = new HashSet<Tusers>();
            InverseLastModifyUser = new HashSet<Tusers>();
            TcategoriesCreatedByUser = new HashSet<Tcategories>();
            TcategoriesLastModifyUser = new HashSet<Tcategories>();
            TemailTemplatesCreatedByUser = new HashSet<TemailTemplates>();
            TemailTemplatesLastModifyUser = new HashSet<TemailTemplates>();
            TfamilyCreatedByUser = new HashSet<Tfamily>();
            TfamilyItemSaleCreatedByUser = new HashSet<TfamilyItemSale>();
            TfamilyItemSaleHistory = new HashSet<TfamilyItemSaleHistory>();
            TfamilyItemSaleLastModifyUser = new HashSet<TfamilyItemSale>();
            TfamilyLastModifyUser = new HashSet<Tfamily>();
            TfamilySaleCreatedByUser = new HashSet<TfamilySale>();
            TfamilySaleLastModifyUser = new HashSet<TfamilySale>();
            TfileSaleCreatedByUser = new HashSet<TfileSale>();
            TfileSaleLastModifyUser = new HashSet<TfileSale>();
            TglobalCreatedByUser = new HashSet<Tglobal>();
            TglobalLastModifyUser = new HashSet<Tglobal>();
            ThashgachaCreatedByUser = new HashSet<Thashgacha>();
            ThashgachaLastModifyUser = new HashSet<Thashgacha>();
            TitemCreatedByUser = new HashSet<Titem>();
            TitemLastModifyUser = new HashSet<Titem>();
            TorderWayCreatedByUser = new HashSet<TorderWay>();
            TorderWayLastModifyUser = new HashSet<TorderWay>();
            TproviderCreatedByUser = new HashSet<Tprovider>();
            TproviderLastModifyUser = new HashSet<Tprovider>();
            Trole = new HashSet<Trole>();
            TsaleCreatedByUser = new HashSet<Tsale>();
            TsaleItemCreatedByUser = new HashSet<TsaleItem>();
            TsaleItemHistoryCreatedByUser = new HashSet<TsaleItemHistory>();
            TsaleItemHistoryLastModifyUser = new HashSet<TsaleItemHistory>();
            TsaleItemLastModifyUser = new HashSet<TsaleItem>();
            TsaleLastModifyUser = new HashSet<Tsale>();
            TsaleStatusCreatedByUser = new HashSet<TsaleStatus>();
            TsaleStatusLastModifyUser = new HashSet<TsaleStatus>();
            TsendTypeCreatedByUser = new HashSet<TsendType>();
            TsendTypeLastModifyUser = new HashSet<TsendType>();
            Tunits = new HashSet<Tunits>();
        }

        public int UserId { get; set; }
        [Required]
        [EmailAddress]
        public string UserName { get; set; }
        [Required][MinLength(6)]
        public string Password { get; set; }
        [Required]
        public int RoleId { get; set; }
        public int CreatedByUserId { get; set; }



        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }


        public Tusers CreatedByUser { get; set; }
        public Tusers LastModifyUser { get; set; }
        public Trole Role { get; set; }
        public ICollection<Tusers> InverseCreatedByUser { get; set; }
        public ICollection<Tusers> InverseLastModifyUser { get; set; }
        public ICollection<Tcategories> TcategoriesCreatedByUser { get; set; }
        public ICollection<Tcategories> TcategoriesLastModifyUser { get; set; }
        public ICollection<TemailTemplates> TemailTemplatesCreatedByUser { get; set; }
        public ICollection<TemailTemplates> TemailTemplatesLastModifyUser { get; set; }
        public ICollection<Tfamily> TfamilyCreatedByUser { get; set; }
        public ICollection<TfamilyItemSale> TfamilyItemSaleCreatedByUser { get; set; }
        public ICollection<TfamilyItemSaleHistory> TfamilyItemSaleHistory { get; set; }
        public ICollection<TfamilyItemSale> TfamilyItemSaleLastModifyUser { get; set; }
        public ICollection<Tfamily> TfamilyLastModifyUser { get; set; }
        public ICollection<TfamilySale> TfamilySaleCreatedByUser { get; set; }
        public ICollection<TfamilySale> TfamilySaleLastModifyUser { get; set; }
        public ICollection<TfileSale> TfileSaleCreatedByUser { get; set; }
        public ICollection<TfileSale> TfileSaleLastModifyUser { get; set; }
        public ICollection<Tglobal> TglobalCreatedByUser { get; set; }
        public ICollection<Tglobal> TglobalLastModifyUser { get; set; }
        public ICollection<Thashgacha> ThashgachaCreatedByUser { get; set; }
        public ICollection<Thashgacha> ThashgachaLastModifyUser { get; set; }
        public ICollection<Titem> TitemCreatedByUser { get; set; }
        public ICollection<Titem> TitemLastModifyUser { get; set; }
        public ICollection<TorderWay> TorderWayCreatedByUser { get; set; }
        public ICollection<TorderWay> TorderWayLastModifyUser { get; set; }
        public ICollection<Tprovider> TproviderCreatedByUser { get; set; }
        public ICollection<Tprovider> TproviderLastModifyUser { get; set; }
        public ICollection<Trole> Trole { get; set; }
        public ICollection<Tsale> TsaleCreatedByUser { get; set; }
        public ICollection<TsaleItem> TsaleItemCreatedByUser { get; set; }
        public ICollection<TsaleItemHistory> TsaleItemHistoryCreatedByUser { get; set; }
        public ICollection<TsaleItemHistory> TsaleItemHistoryLastModifyUser { get; set; }
        public ICollection<TsaleItem> TsaleItemLastModifyUser { get; set; }
        public ICollection<Tsale> TsaleLastModifyUser { get; set; }
        public ICollection<TsaleStatus> TsaleStatusCreatedByUser { get; set; }
        public ICollection<TsaleStatus> TsaleStatusLastModifyUser { get; set; }
        public ICollection<TsendType> TsendTypeCreatedByUser { get; set; }
        public ICollection<TsendType> TsendTypeLastModifyUser { get; set; }
        public ICollection<Tunits> Tunits { get; set; }
    }
}
