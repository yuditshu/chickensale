﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class Titem
    {
        public Titem()
        {
            TsaleItem = new HashSet<TsaleItem>();
            TsaleItemHistory = new HashSet<TsaleItemHistory>();
        }

        public int ItemId { get; set; }
        public string Name { get; set; }
        public string NameInFactory { get; set; }
        public int? CategoryId { get; set; }
        public string BarCode { get; set; }
        public int? HashgachId { get; set; }
        public int? ProviderId { get; set; }
        public int? OrderItems { get; set; }
        public int? OrderCategories { get; set; }
        public decimal? Price { get; set; }
        public int? UnitsInPackage { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }
        public int? ParentLinkId { get; set; }
        public int? UnitId { get; set; }

        public Tcategories Category { get; set; }
        public Tusers CreatedByUser { get; set; }
        public Thashgacha Hashgach { get; set; }
        public Tusers LastModifyUser { get; set; }
        public Tprovider Provider { get; set; }
        public Tunits Unit { get; set; }
        public ICollection<TsaleItem> TsaleItem { get; set; }
        public ICollection<TsaleItemHistory> TsaleItemHistory { get; set; }
    }
}
