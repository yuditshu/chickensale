﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class TsaleItem
    {
        public TsaleItem()
        {
            TfamilyItemSale = new HashSet<TfamilyItemSale>();
            TfamilyItemSaleHistory = new HashSet<TfamilyItemSaleHistory>();
        }

        public int SaleItemId { get; set; }
        public int? SaleId { get; set; }
        public int? ItemId { get; set; }
        public bool? IsCome { get; set; }
        public string FormRemark { get; set; }
        public string SignRemark { get; set; }
        public bool? IfPrintSign { get; set; }
        public int? Extra { get; set; }
        public int? ChangeOrderQuantity { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Titem Item { get; set; }
        public Tusers LastModifyUser { get; set; }
        public Tsale Sale { get; set; }
        public ICollection<TfamilyItemSale> TfamilyItemSale { get; set; }
        public ICollection<TfamilyItemSaleHistory> TfamilyItemSaleHistory { get; set; }
    }
}
