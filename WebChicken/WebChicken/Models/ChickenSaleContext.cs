﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebChicken.Models
{
    public partial class ChickenSaleContext : DbContext
    {
        public ChickenSaleContext()
        {
        }

        public ChickenSaleContext(DbContextOptions<ChickenSaleContext> options)
            : base(options)
        {

        }

        public virtual DbSet<Tcategories> Tcategories { get; set; }
        public virtual DbSet<TemailTemplates> TemailTemplates { get; set; }
        public virtual DbSet<Tfamily> Tfamily { get; set; }
        public virtual DbSet<TfamilyItemSale> TfamilyItemSale { get; set; }
        public virtual DbSet<TfamilyItemSaleHistory> TfamilyItemSaleHistory { get; set; }
        public virtual DbSet<TfamilySale> TfamilySale { get; set; }
        public virtual DbSet<TfileSale> TfileSale { get; set; }
        public virtual DbSet<Tglobal> Tglobal { get; set; }
        public virtual DbSet<Thashgacha> Thashgacha { get; set; }
        public virtual DbSet<Titem> Titem { get; set; }
        public virtual DbSet<TitemHistory> TitemHistory { get; set; }
        public virtual DbSet<TorderWay> TorderWay { get; set; }
        public virtual DbSet<Tprovider> Tprovider { get; set; }
        public virtual DbSet<Trole> Trole { get; set; }
        public virtual DbSet<Tsale> Tsale { get; set; }
        public virtual DbSet<TsaleItem> TsaleItem { get; set; }
        public virtual DbSet<TsaleItemHistory> TsaleItemHistory { get; set; }
        public virtual DbSet<TsaleStatus> TsaleStatus { get; set; }
        public virtual DbSet<TsendType> TsendType { get; set; }
        public virtual DbSet<Tunits> Tunits { get; set; }
        public virtual DbSet<Tusers> Tusers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=sql;Database=ChickenSale;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tcategories>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("TCategories");

                entity.Property(e => e.CategoryId).ValueGeneratedNever();

                entity.Property(e => e.CategoryName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TcategoriesCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TCategori__Creat__5535A963");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TcategoriesLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TCategories_TUsers");
            });

            modelBuilder.Entity<TemailTemplates>(entity =>
            {
                entity.HasKey(e => e.EmailTemplatesId);

                entity.ToTable("TEmailTemplates");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Value).HasMaxLength(3000);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TemailTemplatesCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TEmailTem__Creat__41B8C09B");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TemailTemplatesLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TEmailTem__LastM__43A1090D");
            });

            modelBuilder.Entity<Tfamily>(entity =>
            {
                entity.HasKey(e => e.FamilyId);

                entity.ToTable("TFamily");

               // entity.Property(e => e.FamilyId).ValueGeneratedNever();


                entity.Property(e => e.Address).HasMaxLength(150);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Mail).HasMaxLength(100);

                entity.Property(e => e.Phone).HasMaxLength(100);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Phone2).HasMaxLength(100);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TfamilyCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TFamily__Created__5BE2A6F2");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TfamilyLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TFamily_TUsers");
            });

            modelBuilder.Entity<TfamilyItemSale>(entity =>
            {
                entity.HasKey(e => e.FamilyItemSaleId);

                entity.ToTable("TFamilyItemSale");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TfamilyItemSaleCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TFamilyIt__Creat__1AD3FDA4");

                entity.HasOne(d => d.FamilySale)
                    .WithMany(p => p.TfamilyItemSale)
                    .HasForeignKey(d => d.FamilySaleId)
                    .HasConstraintName("FK__TFamilyIt__Famil__17F790F9");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TfamilyItemSaleLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TFamilyItemSale_TUsers");

                entity.HasOne(d => d.OrderWayNavigation)
                    .WithMany(p => p.TfamilyItemSale)
                    .HasForeignKey(d => d.OrderWay)
                    .HasConstraintName("FK__TFamilyIt__Order__19DFD96B");

                entity.HasOne(d => d.SaleItem)
                    .WithMany(p => p.TfamilyItemSale)
                    .HasForeignKey(d => d.SaleItemId)
                    .HasConstraintName("FK_TFamilyItemSale_TSaleItem");
            });

            modelBuilder.Entity<TfamilyItemSaleHistory>(entity =>
            {
                entity.HasKey(e => e.FamilyItemSaleId);

                entity.ToTable("TFamilyItemSaleHistory");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TfamilyItemSaleHistory)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TFamilyIt__Creat__76619304");

                entity.HasOne(d => d.FamilySale)
                    .WithMany(p => p.TfamilyItemSaleHistory)
                    .HasForeignKey(d => d.FamilySaleId)
                    .HasConstraintName("FK__TFamilyIt__Famil__73852659");

                entity.HasOne(d => d.OrderWayNavigation)
                    .WithMany(p => p.TfamilyItemSaleHistory)
                    .HasForeignKey(d => d.OrderWay)
                    .HasConstraintName("FK__TFamilyIt__Order__756D6ECB");

                entity.HasOne(d => d.SaleItem)
                    .WithMany(p => p.TfamilyItemSaleHistory)
                    .HasForeignKey(d => d.SaleItemId)
                    .HasConstraintName("FK__TFamilyIt__ItemI__74794A92");
            });

            modelBuilder.Entity<TfamilySale>(entity =>
            {
                entity.HasKey(e => e.FamilySaleId);

                entity.ToTable("TFamilySale");

                entity.Property(e => e.FamilySaleId).ValueGeneratedOnAdd();

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreditDebit).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreditDebitCause).HasMaxLength(100);

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TfamilySaleCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TFamilySa__Creat__06CD04F7");

                entity.HasOne(d => d.Family)
                    .WithMany(p => p.TfamilySale)
                    .HasForeignKey(d => d.FamilyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TFamilySa__Famil__04E4BC85");

                entity.HasOne(d => d.FamilySale)
                    .WithOne(p => p.InverseFamilySale)
                    .HasForeignKey<TfamilySale>(d => d.FamilySaleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TFamilySale_TFamilySale");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TfamilySaleLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TFamilySale_TUsers");

                entity.HasOne(d => d.Sale)
                    .WithMany(p => p.TfamilySale)
                    .HasForeignKey(d => d.SaleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TFamilySa__SaleI__05D8E0BE");
            });

            modelBuilder.Entity<TfileSale>(entity =>
            {
                entity.HasKey(e => e.FileSaleId);

                entity.ToTable("TFileSale");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Link).HasMaxLength(250);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TfileSaleCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TFileSale__Creat__29221CFB");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TfileSaleLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TFileSale_TUsers");

                entity.HasOne(d => d.Provider)
                    .WithMany(p => p.TfileSale)
                    .HasForeignKey(d => d.ProviderId)
                    .HasConstraintName("FK__TFileSale__Provi__282DF8C2");

                entity.HasOne(d => d.Sale)
                    .WithMany(p => p.TfileSale)
                    .HasForeignKey(d => d.SaleId)
                    .HasConstraintName("FK__TFileSale__SaleI__2739D489");
            });

            modelBuilder.Entity<Tglobal>(entity =>
            {
                entity.HasKey(e => e.GlobalId);

                entity.ToTable("TGlobal");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.OldValue).HasMaxLength(100);

                entity.Property(e => e.SysRowStatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TglobalCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TGlobal__Created__2EA5EC27");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TglobalLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK__TGlobal__LastMod__308E3499");
            });

            modelBuilder.Entity<Thashgacha>(entity =>
            {
                entity.HasKey(e => e.HashgachId);

                entity.ToTable("THashgacha");

                entity.Property(e => e.Color).HasMaxLength(100);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NameHashgach).HasMaxLength(100);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.ThashgachaCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__THashgach__Creat__4BAC3F29");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.ThashgachaLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_THashgacha_TUsers");
            });

            modelBuilder.Entity<Titem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("TItem");

                entity.HasIndex(e => e.ItemId)
                    .HasName("IX_TItem");

                entity.Property(e => e.ItemId).ValueGeneratedNever();

                entity.Property(e => e.BarCode).HasMaxLength(100);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.NameInFactory).HasMaxLength(100);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Titem)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_TProduct_TCategories");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TitemCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TItem_TUsers1");

                entity.HasOne(d => d.Hashgach)
                    .WithMany(p => p.Titem)
                    .HasForeignKey(d => d.HashgachId)
                    .HasConstraintName("FK_TProduct_THashgacha");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TitemLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TItem_TUsers");

                entity.HasOne(d => d.Provider)
                    .WithMany(p => p.Titem)
                    .HasForeignKey(d => d.ProviderId)
                    .HasConstraintName("FK_TItem_TProvider");

                entity.HasOne(d => d.Unit)
                    .WithMany(p => p.Titem)
                    .HasForeignKey(d => d.UnitId)
                    .HasConstraintName("FK__TItem__parentLin__4865BE2A");
            });

            modelBuilder.Entity<TitemHistory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("TItemHistory");

                entity.Property(e => e.ItemId).ValueGeneratedNever();

                entity.Property(e => e.BarCode).HasMaxLength(100);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.NameInFactory).HasMaxLength(100);

                entity.HasOne(d => d.Unit)
                    .WithMany(p => p.TitemHistory)
                    .HasForeignKey(d => d.UnitId)
                    .HasConstraintName("FK_TItemHistory_TUnits");
            });

            modelBuilder.Entity<TorderWay>(entity =>
            {
                entity.HasKey(e => e.OrderWayId);

                entity.ToTable("TOrderWay");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Value).HasMaxLength(100);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TorderWayCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TOrderWay__Creat__7D439ABD");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TorderWayLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TOrderWay_TUsers");
            });

            modelBuilder.Entity<Tprovider>(entity =>
            {
                entity.HasKey(e => e.ProviderId);

                entity.ToTable("TProvider");

                entity.Property(e => e.ProviderId).ValueGeneratedOnAdd();

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FaxHome1).HasMaxLength(100);

                entity.Property(e => e.FaxHome2).HasMaxLength(100);

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Mail).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Phone1)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Phone2).HasMaxLength(100);

                entity.Property(e => e.Remark).HasMaxLength(100);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TproviderCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TProvider__Creat__236943A5");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TproviderLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TProvider_TUsers");

                entity.HasOne(d => d.Provider)
                    .WithOne(p => p.InverseProvider)
                    .HasForeignKey<Tprovider>(d => d.ProviderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TProvider_TProvider");
            });

            modelBuilder.Entity<Trole>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("TRole");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.RoleType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SysRowStatus).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.Trole)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TRole_TUsers");
            });

            modelBuilder.Entity<Tsale>(entity =>
            {
                entity.HasKey(e => e.SaleId);

                entity.ToTable("TSale");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.FinishDate).HasColumnType("datetime");

                entity.Property(e => e.LastCheckDate).HasColumnType("datetime");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NextSaleDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TsaleCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TSale__CreatedBy__72C60C4A");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TsaleLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TSale_TUsers");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Tsale)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__TSale__StatusId__71D1E811");
            });

            modelBuilder.Entity<TsaleItem>(entity =>
            {
                entity.HasKey(e => e.SaleItemId);

                entity.ToTable("TSaleItem");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FormRemark).HasMaxLength(250);

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.SignRemark).HasMaxLength(250);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TsaleItemCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TSaleItem__Creat__31B762FC");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.TsaleItem)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_TSaleItem_TItem");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TsaleItemLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK__TSaleItem__LastM__339FAB6E");

                entity.HasOne(d => d.Sale)
                    .WithMany(p => p.TsaleItem)
                    .HasForeignKey(d => d.SaleId)
                    .HasConstraintName("FK__TSaleItem__SaleI__2FCF1A8A");
            });

            modelBuilder.Entity<TsaleItemHistory>(entity =>
            {
                entity.HasKey(e => e.SaleItemId);

                entity.ToTable("TSaleItemHistory");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FormRemark).HasMaxLength(250);

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.SignRemark).HasMaxLength(250);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TsaleItemHistoryCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TSaleItem__Creat__6EC0713C");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.TsaleItemHistory)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK__TSaleItem__ItemI__6DCC4D03");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TsaleItemHistoryLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK__TSaleItem__LastM__70A8B9AE");

                entity.HasOne(d => d.Sale)
                    .WithMany(p => p.TsaleItemHistory)
                    .HasForeignKey(d => d.SaleId)
                    .HasConstraintName("FK__TSaleItem__SaleI__6CD828CA");
            });

            modelBuilder.Entity<TsaleStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId);

                entity.ToTable("TSaleStatus");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.StatusName).HasMaxLength(20);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TsaleStatusCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TSaleStat__Creat__6A30C649");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TsaleStatusLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TSaleStatus_TUsers");
            });

            modelBuilder.Entity<TsendType>(entity =>
            {
                entity.HasKey(e => e.SendTypeId);

                entity.ToTable("TSendType");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.TsendTypeCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TSendType__Creat__5E8A0973");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.TsendTypeLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK__TSendType__LastM__607251E5");
            });

            modelBuilder.Entity<Tunits>(entity =>
            {
                entity.HasKey(e => e.UnitId);

                entity.ToTable("TUnits");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Plural).HasMaxLength(100);

                entity.Property(e => e.Singular).HasMaxLength(100);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.Tunits)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TUnits__CreatedB__4F7CD00D");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.InverseLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TUnits_TUnits");
            });

            modelBuilder.Entity<Tusers>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("TUsers");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifyDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Password).HasMaxLength(200);

                entity.Property(e => e.SysRowStatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.InverseCreatedByUser)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .HasConstraintName("FK__TUsers__CreatedB__45F365D3");

                entity.HasOne(d => d.LastModifyUser)
                    .WithMany(p => p.InverseLastModifyUser)
                    .HasForeignKey(d => d.LastModifyUserId)
                    .HasConstraintName("FK_TUsers_TUsers");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Tusers)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__TUsers__RoleId__44FF419A");
            });
        }
    }
}
