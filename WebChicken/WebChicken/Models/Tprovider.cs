﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebChicken.Models
{
    public partial class Tprovider
    {
        public Tprovider()
        {
            TfileSale = new HashSet<TfileSale>();
            Titem = new HashSet<Titem>();
        }
        [Required]
        public int ProviderId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [MaxLength(20)]
        [Phone(ErrorMessage ="The phone is incorrect")]
        public string Phone1 { get; set; }
        [MaxLength(20)]
        [Phone(ErrorMessage = "The phone is incorrect")]
        public string Phone2 { get; set; }
        [MaxLength(20)]
        [Phone(ErrorMessage = "The Fax is incorrect")]
        public string FaxHome1 { get; set; }
        [MaxLength(20)]
        [Phone(ErrorMessage = "The Fax is incorrect")]
        public string FaxHome2 { get; set; }
        [EmailAddress]
        public string Mail { get; set; }
        [MaxLength(100)]
        public string Remark { get; set; }
        public int? SendTypeId { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate{ get; set; }
        public int SysRowStatus { get; set; }
        public Tusers CreatedByUser { get; set; }
        public Tusers LastModifyUser { get; set; }
        public Tprovider Provider { get; set; }
        public Tprovider InverseProvider { get; set; }
        public ICollection<TfileSale> TfileSale { get; set; }
        public ICollection<Titem> Titem { get; set; }
    }
}
