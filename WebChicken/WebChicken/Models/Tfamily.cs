﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebChicken.Models
{
    public partial class Tfamily
    {
        public Tfamily()
        {
            TfamilySale = new HashSet<TfamilySale>();
        }

        public int FamilyId { get; set; }
        [Required]
        [Phone]
        public string Phone { get; set; }
        public string Name { get; set; }
        [Phone]
        public string Phone2 { get; set; }
        [EmailAddress]
        public string Mail { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsOnlyMail { get; set; }
        public string Address { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }
        public int? FamilyIdPhone { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Tusers LastModifyUser { get; set; }
        public ICollection<TfamilySale> TfamilySale { get; set; }
    }
}

