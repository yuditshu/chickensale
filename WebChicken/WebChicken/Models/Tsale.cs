﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class Tsale
    {
        public Tsale()
        {
            TfamilySale = new HashSet<TfamilySale>();
            TfileSale = new HashSet<TfileSale>();
            TsaleItem = new HashSet<TsaleItem>();
            TsaleItemHistory = new HashSet<TsaleItemHistory>();
        }

        public int SaleId { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? LastCheckDate { get; set; }
        public DateTime? NextSaleDate { get; set; }
        public int? StatusId { get; set; }
        public DateTime? FinishDate { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Tusers LastModifyUser { get; set; }
        public TsaleStatus Status { get; set; }
        public ICollection<TfamilySale> TfamilySale { get; set; }
        public ICollection<TfileSale> TfileSale { get; set; }
        public ICollection<TsaleItem> TsaleItem { get; set; }
        public ICollection<TsaleItemHistory> TsaleItemHistory { get; set; }
    }
}
