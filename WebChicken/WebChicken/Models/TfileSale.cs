﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class TfileSale
    {
        public int FileSaleId { get; set; }
        public int? SaleId { get; set; }
        public int? ProviderId { get; set; }
        public string Link { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Tusers LastModifyUser { get; set; }
        public Tprovider Provider { get; set; }
        public Tsale Sale { get; set; }
    }
}
