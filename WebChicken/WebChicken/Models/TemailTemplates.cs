﻿using System;
using System.Collections.Generic;
namespace WebChicken.Models
{
    using System;
    using System.Collections.Generic;

    public partial class TemailTemplates
    {
        public int EmailTemplatesId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Tusers LastModifyUser { get; set; }
    }
}
