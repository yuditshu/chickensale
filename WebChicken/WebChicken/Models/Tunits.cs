﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class Tunits
    {
        public Tunits()
        {
            InverseLastModifyUser = new HashSet<Tunits>();
            Titem = new HashSet<Titem>();
            TitemHistory = new HashSet<TitemHistory>();
        }

        public int UnitId { get; set; }
        public bool? IsWeight { get; set; }
        public string Singular { get; set; }
        public string Plural { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Tunits LastModifyUser { get; set; }
        public ICollection<Tunits> InverseLastModifyUser { get; set; }
        public ICollection<Titem> Titem { get; set; }
        public ICollection<TitemHistory> TitemHistory { get; set; }
    }
}
