﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class TorderWay
    {
        public TorderWay()
        {
            TfamilyItemSale = new HashSet<TfamilyItemSale>();
            TfamilyItemSaleHistory = new HashSet<TfamilyItemSaleHistory>();
        }

        public int OrderWayId { get; set; }
        public string Value { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Tusers LastModifyUser { get; set; }
        public ICollection<TfamilyItemSale> TfamilyItemSale { get; set; }
        public ICollection<TfamilyItemSaleHistory> TfamilyItemSaleHistory { get; set; }
    }
}
