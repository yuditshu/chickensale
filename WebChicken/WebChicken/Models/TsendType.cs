﻿using System;
using System.Collections.Generic;

namespace WebChicken.Models
{
    public partial class TsendType
    {
        public int SendTypeId { get; set; }
        public string Value { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int SysRowStatus { get; set; }

        public Tusers CreatedByUser { get; set; }
        public Tusers LastModifyUser { get; set; }
    }
}
