﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebChicken.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IO;
using NLog;
using WebChicken.Helpers;

namespace WebChicken
{
    public class Startup
    {

        //put secret here for simplicity, usually it should be in appsettings.json
        public const string SECRET = "GQDstc21ewfffffffffffFiwDffVvVBrk";

        public Startup(IConfiguration configuration)
        {
            LogManager.LoadConfiguration(System.String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<ChickenSaleContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("ChickenSaleDataBase")));
           
         
            var key = Encoding.ASCII.GetBytes(SECRET);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = true;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddSingleton<ILog, LogNLog>();


            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling =
                                           Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {


           // Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
            //loggerFactory.AddLog4Net(@"log4net.config");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(builder => builder
.AllowAnyOrigin()
 .AllowAnyMethod()
 .AllowAnyHeader()
.AllowCredentials());
            }
            else
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(builder => builder
.AllowAnyOrigin()
 .AllowAnyMethod()
 .AllowAnyHeader()
.AllowCredentials());
                app.UseHsts();
            }
           

            //app.UseHttpsRedirection();
            app.UseAuthentication();


            app.UseMvc();

        }


    }
}
