﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Net.Sockets;

using EAGetMail; //add EAGetMail namespace

namespace ReadEmail
{
    class ReadEmailsFromInbox
    {
       public static void readEmails()
       {
            try
            {
                // Create a folder named "inbox" under current directory
                // to save the email retrieved.
                string localInbox = string.Format(@"C:/WebChicken/ReadEmail/filesFromEmail");
                //, Directory.GetCurrentDirectory()
                // If the folder is not existed, create it.
                if (!Directory.Exists(localInbox))
                {
                    Directory.CreateDirectory(localInbox);
                }

                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "chickensale1234@gmail.com",
                                "ch1234sale",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);
                MailInfo[] infos = oClient.GetMailInfos();
                Console.WriteLine("Total {0} email(s)\r\n", infos.Length);
                for (int i = 0; i < infos.Length; i++)
                {
                    MailInfo info = infos[i];
                    if (info.IMAP4MailFlags.ToString() != "(\\Seen)" && info.IMAP4MailFlags.ToString() != "(\\Answered \\Seen)")
                    {
                        Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",
                                            info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        //לקיחת הקובץ אקסל מהמייל
                        if (oMail.Attachments.Count() > 0)
                        {
                            string nameFile = DateTime.Now.Ticks + "__" + oMail.From.Address + ".xlsx";
                            string fullPath1 = string.Format("{0}//{1}", localInbox, nameFile);
                            oMail.Attachments.First().SaveAs(fullPath1, true);

                            //קריאה לסרביס
                            //שולח מייל ושם קובץ
                            connectToService.RunAsync(oMail.From.Address, nameFile).Wait();
                        }
                        Console.WriteLine("From: {0}", oMail.From.ToString());
                        Console.WriteLine("Subject: {0}\r\n", oMail.Subject);
                        // Mark email as Read
                        oClient.MarkAsRead(info, true);
                    }
                }
                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

        }


    }
}
